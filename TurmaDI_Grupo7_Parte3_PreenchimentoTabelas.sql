-- Preencher Tabelas    

-- Tabela zona_geografica

INSERT INTO zona_geografica VALUES ('N1', 'Porto');
INSERT INTO zona_geografica VALUES ('C1', 'Lisboa');
INSERT INTO zona_geografica VALUES ('S1', 'Faro');

-- Tabela armazem

INSERT INTO armazem VALUES (1, 'N1', 'Armazem 1', 'Avenida Central', 12.53367, 98.32563);
INSERT INTO armazem VALUES (2, 'N1', 'Armazem 2', 'Rua Central', 43.18929, 123.38255);
INSERT INTO armazem VALUES (3, 'C1', 'Armazem 3', 'Rua Dr. Carlos Amaral', 75.15268, 12.35254);
INSERT INTO armazem VALUES (4, 'C1', 'Armazem 4', 'Rua D. Maria', 21.36742, 85.12432);
INSERT INTO armazem VALUES (5, 'S1', 'Armazem 5', 'Avenida Dr. Jos� R�gio', 86.23453, 156.24642);
INSERT INTO armazem VALUES (6, 'S1', 'Armazem 6', 'Rua dos Milagres', 21.234322, 25.44222);

-- Tabela zona_fisica

INSERT INTO zona_fisica VALUES ('Zona A1', 1, 123);
INSERT INTO zona_fisica VALUES ('Zona B1', 1, 760);
INSERT INTO zona_fisica VALUES ('Zona A2', 2, 532);
INSERT INTO zona_fisica VALUES ('Zona B2', 2, 49);
INSERT INTO zona_fisica VALUES ('Zona C2', 2, 324);
INSERT INTO zona_fisica VALUES ('Zona A3', 3, 234);
INSERT INTO zona_fisica VALUES ('Zona A4', 4, 124);
INSERT INTO zona_fisica VALUES ('Zona B4', 4, 532);
INSERT INTO zona_fisica VALUES ('Zona A5', 5, 97);
INSERT INTO zona_fisica VALUES ('Zona A6', 6, 221);
INSERT INTO zona_fisica VALUES ('Zona B6', 6, 154);

-- Table artigo

INSERT INTO artigo VALUES ('Artigo 1', 'Mesa', 'Mesa de madeira de carvalho', 56, 89, 'unidade', 32, 23);
INSERT INTO artigo VALUES ('Artigo 2', 'Cadeira', 'Cadeira em pele', 78, 110, 'unidade', 56, 12);
INSERT INTO artigo VALUES ('Artigo 3', 'Prafusos', 'Caixas de 50 parafusos', 2.50, 9.50, 'caixa', 1, 0.5);
INSERT INTO artigo VALUES ('Artigo 4', 'Tinta', 'Tinta de parede', 12, 25, 'l', 5, 2);

-- Tabela zona_fisica_artigo

INSERT INTO zona_fisica_artigo VALUES ('Zona A1', 'Artigo 1', 1, 95);
INSERT INTO zona_fisica_artigo VALUES ('Zona B1', 'Artigo 1', 1, 70);
INSERT INTO zona_fisica_artigo VALUES ('Zona B2', 'Artigo 2', 2, 24);
INSERT INTO zona_fisica_artigo VALUES ('Zona A2', 'Artigo 1', 2, 35);
INSERT INTO zona_fisica_artigo VALUES ('Zona B2', 'Artigo 4', 2, 156);
INSERT INTO zona_fisica_artigo VALUES ('Zona C2', 'Artigo 2', 2, 156);
INSERT INTO zona_fisica_artigo VALUES ('Zona A3', 'Artigo 4', 3, 0);
INSERT INTO zona_fisica_artigo VALUES ('Zona A4', 'Artigo 3', 4, 45);
INSERT INTO zona_fisica_artigo VALUES ('Zona B4', 'Artigo 4', 4, 402);
INSERT INTO zona_fisica_artigo VALUES ('Zona A5', 'Artigo 3', 5, 353);
INSERT INTO zona_fisica_artigo VALUES ('Zona A6', 'Artigo 2', 6, 67);
INSERT INTO zona_fisica_artigo VALUES ('Zona B6', 'Artigo 2', 6, 32);

-- Tabela historico_mudanca_preco

INSERT INTO historico_mudanca_preco VALUES (DATE '2015-12-17', 'Artigo 1', DATE'2017-09-01', 102);
INSERT INTO historico_mudanca_preco VALUES (DATE '2016-08-12', 'Artigo 2', DATE'2018-09-16', 156);
INSERT INTO historico_mudanca_preco VALUES (DATE '2017-02-17', 'Artigo 2', DATE'2018-05-20', 5);
INSERT INTO historico_mudanca_preco VALUES (DATE '2014-11-17', 'Artigo 4', DATE'2015-03-22', 43);

-- Tabela stock_artigo

INSERT INTO stock_artigo VALUES (1, 'Artigo 1', 5);
INSERT INTO stock_artigo VALUES (2, 'Artigo 1', 21);
INSERT INTO stock_artigo VALUES (3, 'Artigo 1', 43);
INSERT INTO stock_artigo VALUES (4, 'Artigo 1', 12);
INSERT INTO stock_artigo VALUES (5, 'Artigo 1', 6);
INSERT INTO stock_artigo VALUES (6, 'Artigo 1', 32);
INSERT INTO stock_artigo VALUES (1, 'Artigo 2', 32);
INSERT INTO stock_artigo VALUES (2, 'Artigo 2', 21);
INSERT INTO stock_artigo VALUES (3, 'Artigo 2', 12);
INSERT INTO stock_artigo VALUES (4, 'Artigo 2', 4);
INSERT INTO stock_artigo VALUES (5, 'Artigo 2', 9);
INSERT INTO stock_artigo VALUES (6, 'Artigo 2', 2);
INSERT INTO stock_artigo VALUES (1, 'Artigo 3', 7);
INSERT INTO stock_artigo VALUES (2, 'Artigo 3', 13);
INSERT INTO stock_artigo VALUES (3, 'Artigo 3', 56);
INSERT INTO stock_artigo VALUES (4, 'Artigo 3', 25);
INSERT INTO stock_artigo VALUES (5, 'Artigo 3', 26);
INSERT INTO stock_artigo VALUES (6, 'Artigo 3', 8);
INSERT INTO stock_artigo VALUES (1, 'Artigo 4', 4);
INSERT INTO stock_artigo VALUES (2, 'Artigo 4', 8);
INSERT INTO stock_artigo VALUES (3, 'Artigo 4', 43);
INSERT INTO stock_artigo VALUES (4, 'Artigo 4', 7);
INSERT INTO stock_artigo VALUES (5, 'Artigo 4', 12);
INSERT INTO stock_artigo VALUES (6, 'Artigo 4', 42);

-- Tabela categoria

INSERT INTO categoria VALUES ('M', 'Motorista');
INSERT INTO categoria VALUEs ('V', 'Vendedor');

-- Tabela funcionario

INSERT INTO funcionario VALUES (1, null, 1, 'M', 'Rua da Igreja', 560, 124385677, 'Manuel Ferreira',  214632743);
INSERT INTO funcionario VALUES (2, null, 2, 'M', 'Rua Central', 750, 625629424, 'Joaquim Silva',  432574554);
INSERT INTO funcionario VALUES (3, 2, 3, 'M', 'Avenida da Esperan�a', 800, 8439342, 'M�nica Costa',  245213579);
INSERT INTO funcionario VALUES (4, 1, 4, 'M', 'Rua dos Anjos', 780, 135342224, 'Jos� Marques',  234790743);
INSERT INTO funcionario VALUES (5, 2, 5, 'M', 'Rua da Miseric�rdia', 900, 274937463, 'Sara Castro',  344632446);
INSERT INTO funcionario VALUES (6, 1, 6, 'M', 'Avenida Alegria', 1010, 134737964, 'Alberto Sousa',  245354643);
INSERT INTO funcionario VALUES (7, null, 1, 'V', 'Rua Dr. Magalh�es', 790, 237459753, 'Raquel Silva',  245353654);
INSERT INTO funcionario VALUES (8, null, 2, 'V', 'Rua Dona Rosa', 1500, 314575327, 'Sandra Ramos',  246853675);
INSERT INTO funcionario VALUES (9, 7, 3, 'V', 'Avenida do Sacramento', 400, 13586242, 'Miguel Costa',  245354353);
INSERT INTO funcionario VALUES (10, 8, 4, 'V', 'Rua do Monte', 800, 253678346, 'Carlos Silva',  048735864);
INSERT INTO funcionario VALUES (11, 8, 5, 'V', 'Rua do Desespero', 1005, 359864124, 'Tiago Roque',  134647469);
INSERT INTO funcionario VALUES (12, 7, 6, 'V', 'Avenida Dr. Carlos Ferreira', 950, 135970533, 'Mariana Silva',  22145687);

-- Tabela info_isepbricolage

INSERT INTO info_isepbricolage VALUES (1, 'Isep Bricolage', 'Avenida da Boavista', 4640264, 'Porto', 252435322, 13624253, 236437826);

-- Tabela transporte_mercadorias

INSERT INTO transporte_mercadorias VALUES ('N1', 1, 140);
INSERT INTO transporte_mercadorias VALUES ('N1', 2, 15);
INSERT INTO transporte_mercadorias VALUES ('C1', 3, 156);
INSERT INTO transporte_mercadorias VALUES ('C1', 4, 58);
INSERT INTO transporte_mercadorias VALUES ('S1', 5, 89);
INSERT INTO transporte_mercadorias VALUES ('S1', 6, 116);

-- Tabela tipo_cliente

INSERT INTO tipo_cliente VALUES ('VIP', 'VIP', 50000);
INSERT INTO tipo_cliente VALUES ('GC', 'Grande Cliente', 30000);
INSERT INTO tipo_cliente VALUES ('PC', 'Pequeno Cliente', -1);

-- Tabela cliente

INSERT INTO cliente VALUES (1, 'N1', 'VIP', 'Raquel Moreira', 'Rua do Monte', 2453532, 965685346, 135235213, 70000);
INSERT INTO cliente VALUES (2, 'C1', 'GC', 'Sara Silva', 'Avenida das Alminhas', 532532, 913463787, 256347907, 45000);
INSERT INTO cliente VALUES (3, 'S1', 'PC', 'Joaquim Fonseca', 'Rua D. Maria', 4623433, 935735146, 123643754, 1500);
INSERT INTO cliente VALUES (4, 'S1', 'PC', 'Joaquim Fonseca', 'Rua D. Maria', 4623433, 935735146, 123643754, 1500);
INSERT INTO cliente VALUES (5, 'S1', 'PC', 'Joaquim Fonseca', 'Rua D. Maria', 4623433, 935735146, 123643754, 1500);

-- Tabela fatura

INSERT INTO fatura VALUES (890, 1, 1, 1, TO_DATE('15/06/2010', 'DD/MM/YYYY'), 17000);
INSERT INTO fatura VALUES (900, 1, 1, 1, TO_DATE('15/07/2010', 'DD/MM/YYYY'), 19000);
INSERT INTO fatura VALUES (923, 1, 1, 1, TO_DATE('15/02/2011', 'DD/MM/YYYY'), 20000);
INSERT INTO fatura VALUES (950, 1, 1, 1, TO_DATE('15/03/2011', 'DD/MM/YYYY'), 15000);
INSERT INTO fatura VALUES (1021, 2, 1, 1, TO_DATE('15/03/2012', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (940, 2, 1, 1, TO_DATE('15/03/2011', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (1040, 2, 1, 1, TO_DATE('15/06/2012', 'DD/MM/YYYY'), 9000);
INSERT INTO fatura VALUES (1050, 2, 1, 1, TO_DATE('15/07/2012', 'DD/MM/YYYY'), 10500);
INSERT INTO fatura VALUES (1060, 3, 1, 1, TO_DATE('15/07/2018', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (1070, 3, 1, 1, TO_DATE('15/07/2018', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (1080, 3, 1, 1, TO_DATE('15/04/2018', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (1090, 3, 1, 1, TO_DATE('15/09/2018', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (321, 4, 1, 1, TO_DATE('15/11/2018', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (432, 4, 1, 1, TO_DATE('15/10/2018', 'DD/MM/YYYY'), 10000);
INSERT INTO fatura VALUES (348, 4, 1, 1, TO_DATE('15/09/2018', 'DD/MM/YYYY'), 10000);

-- Tabela linha_fatura

-- Tabela pagamento

INSERT INTO pagamento VALUES (923, 5000);
INSERT INTO pagamento VALUES (940, 3000);

-- Tabela estado

INSERT INTO estado VALUES ('PR', 'Processada');
INSERT INTO estado VALUES ('PE', 'Pendente');

-- Tabela nota_encomenda

INSERT INTO nota_encomenda VALUES ('NE1', 7, 1, 'PR', DATE '2018-11-25');
INSERT INTO nota_encomenda VALUES ('NE2', 8, 2, 'PE', DATE '2018-12-23');
INSERT INTO nota_encomenda VALUES ('NE3', 9, 1, 'PE', DATE '2018-08-12');
INSERT INTO nota_encomenda VALUES ('NE4', 10, 1, 'PE', DATE '2018-05-16');
INSERT INTO nota_encomenda VALUES ('NE5', 11, 1, 'PE', DATE '2018-09-01');
INSERT INTO nota_encomenda VALUES ('NE6', 12, 2, 'PE', DATE '2018-04-23');
INSERT INTO nota_encomenda VALUES ('NE7', 7, 2, 'PE', DATE '2018-10-16');
INSERT INTO nota_encomenda VALUES ('NE8', 9, 3, 'PE', DATE '2018-01-19');
INSERT INTO nota_encomenda VALUES ('NE9', 7, 3, 'PE', DATE '2018-10-02');
INSERT INTO nota_encomenda VALUES ('NE10', 7, 2, 'PE', DATE '2018-10-17');
INSERT INTO nota_encomenda VALUES ('NE11', 7, 2, 'PE', DATE '2018-10-21');
INSERT INTO nota_encomenda VALUES ('NE12', 7, 2, 'PE', DATE '2018-02-21');
INSERT INTO nota_encomenda VALUES ('NE13', 7, 2, 'PE', DATE '2018-02-28');
INSERT INTO nota_encomenda VALUES ('NE14', 7, 2, 'PE', DATE '2018-03-07');

-- Tabela artigo_encomenda

INSERT INTO artigo_encomenda VALUES ('Artigo 1', 'NE1', 2);
INSERT INTO artigo_encomenda VALUES ('Artigo 2', 'NE2', 10);
INSERT INTO artigo_encomenda VALUES ('Artigo 4', 'NE2', 19);
INSERT INTO artigo_encomenda VALUES ('Artigo 3', 'NE2', 200);
INSERT INTO artigo_encomenda VALUES ('Artigo 1', 'NE3', 50);
INSERT INTO artigo_encomenda VALUES ('Artigo 4', 'NE4', 12);
INSERT INTO artigo_encomenda VALUES ('Artigo 3', 'NE5', 26);
INSERT INTO artigo_encomenda VALUES ('Artigo 2', 'NE6', 44);
INSERT INTO artigo_encomenda VALUES ('Artigo 1', 'NE6', 12);
INSERT INTO artigo_encomenda VALUES ('Artigo 4', 'NE7', 6);
INSERT INTO artigo_encomenda VALUES ('Artigo 2', 'NE8', 12);
INSERT INTO artigo_encomenda VALUES ('Artigo 4', 'NE9', 56);
INSERT INTO artigo_encomenda VALUES ('Artigo 3', 'NE9', 150);
INSERT INTO artigo_encomenda VALUES ('Artigo 1', 'NE10', 400);
INSERT INTO artigo_encomenda VALUES ('Artigo 2', 'NE10', 600);
INSERT INTO artigo_encomenda VALUES ('Artigo 3', 'NE10', 500);

-- Tabela guia_saida

INSERT INTO guia_saida (id_nota_encomenda) VALUES ('NE1');

--- Tabela guia_artigo_zona

INSERT INTO guia_artigo_zona VALUES (1, 'Zona A1', 'Artigo 1', 1, 2);

-- Tabela atualizacao_tipo_cliente

INSERT INTO atualizacao_tipo_cliente VALUES (DATE '2016-12-23', 2, 'VIP');
INSERT INTO atualizacao_tipo_cliente VALUES (DATE '2017-08-12', 1, 'GC');
INSERT INTO atualizacao_tipo_cliente VALUES (DATE '2016-08-12', 3, 'PC');

-- Tabela marca

INSERT INTO marca VALUES ('O', 'Opel', 961236473);
INSERT INTO marca VALUES ('R', 'Renault', 914353852);
INSERT INTO marca VALUES ('S', 'Suzuki', 961353890);
INSERT INTO marca VALUES ('P', 'Peugeot', 936538243);

-- Tabela modelo

INSERT INTO modelo VALUES ('A1', 'O', '4 eixos', 100, 500);
INSERT INTO modelo VALUES ('A2', 'R', '4 eixos', 100, 500);
INSERT INTO modelo VALUES ('A3', 'S', '2 eixos', 20, 30);
INSERT INTO modelo VALUES ('A4', 'P', '3 eixos', 50, 70);

-- Tabela veiculo

INSERT INTO veiculo VALUES ('14-GH-55', 'A1', 125353, 2563.4, 1);
INSERT INTO veiculo VALUES ('32-SR-65', 'A1', 235365, 24.4, 1);
INSERT INTO veiculo VALUES ('97-RG-74', 'A2', 294932, 324, 1);
INSERT INTO veiculo VALUES ('12-FE-34', 'A2', 276249, 733, 0);
INSERT INTO veiculo VALUES ('45-OJ-35', 'A3', 274381, 63, 1);
INSERT INTO veiculo VALUES ('23-GR-98', 'A4', 191312, 2437.6, 1);

-- Tabela viagem

INSERT INTO viagem VALUES (1, '14-GH-55', DATE '2018-02-27');
INSERT INTO viagem VALUES (2, '32-SR-65', DATE '2018-01-30');
INSERT INTO viagem VALUES (3, '97-RG-74', DATE '2018-09-17');
INSERT INTO viagem VALUES (4, '12-FE-34', DATE '2018-11-29');
INSERT INTO viagem VALUES (5, '45-OJ-35', DATE '2018-08-13');
INSERT INTO viagem VALUES (6, '23-GR-98', DATE '2018-04-26');
INSERT INTO viagem VALUES (7, '23-GR-98', DATE '2018-11-29');
INSERT INTO viagem VALUES (8, '23-GR-98', DATE '2018-11-30');
INSERT INTO viagem VALUES (9, '23-GR-98', DATE '2018-12-01');
INSERT INTO viagem VALUES (10, '23-GR-98', DATE '2018-12-01');

-- Tabela etapa_viagem

INSERT INTO etapa_viagem VALUES (1, 1);
INSERT INTO etapa_viagem VALUES (2, 1);
INSERT INTO etapa_viagem VALUES (3, 1);
INSERT INTO etapa_viagem VALUES (4, 1);
INSERT INTO etapa_viagem VALUES (1, 2);
INSERT INTO etapa_viagem VALUES (1, 3);
INSERT INTO etapa_viagem VALUES (2, 3);
INSERT INTO etapa_viagem VALUES (3, 3);
INSERT INTO etapa_viagem VALUES (1, 4);
INSERT INTO etapa_viagem VALUES (1, 5);
INSERT INTO etapa_viagem VALUES (1, 6);
INSERT INTO etapa_viagem VALUES (1, 7);
INSERT INTO etapa_viagem VALUES (2, 7);
INSERT INTO etapa_viagem VALUES (3, 7);

-- Tabela guia_transporte

INSERT INTO guia_transporte VALUES ('NE1', 1, 1);
INSERT INTO guia_transporte VALUES ('NE2', 1, 2);
INSERT INTO guia_transporte VALUES ('NE3', 2, 1);
INSERT INTO guia_transporte VALUES ('NE4', 3, 1);
INSERT INTO guia_transporte VALUES ('NE5', 3, 2);
INSERT INTO guia_transporte VALUES ('NE6', 3, 3);
INSERT INTO guia_transporte VALUES ('NE7', 4, 1);
INSERT INTO guia_transporte VALUES ('NE8', 5, 1);
INSERT INTO guia_transporte VALUES ('NE9', 6, 1);
INSERT INTO guia_transporte VALUES ('NE10', 1, 3);
INSERT INTO guia_transporte VALUES ('NE11', 1, 4);
INSERT INTO guia_transporte VALUES ('NE12', 7, 1);
INSERT INTO guia_transporte VALUES ('NE13', 7, 2);
INSERT INTO guia_transporte VALUES ('NE14', 7, 3);
