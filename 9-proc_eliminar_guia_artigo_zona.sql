SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE proc_eliminar_guia_artigo_zona(p_id_guia_saida IN guia_saida.id_guia_saida%TYPE)
IS
    -- Cursor que devolve todas as guia_artigo zona cujo id_guia_saida corresponde ao id recebido como par�metro
    CURSOR cur_guia_artigo_zona(p_id_guia_saida IN guia_saida.id_guia_saida%TYPE) IS
        SELECT *
        FROM guia_artigo_zona GAZ
        WHERE GAZ.id_guia_saida = p_id_guia_saida;
            
    l_guia_artigo_zona cur_guia_artigo_zona%ROWTYPE;
  
BEGIN
   
    -- Para cada guia_artigo_zona correspondente � guia_saida recebida por par�metro, ser�o atualizados os stocks das
    -- respetivas zonas fisicas artigo seguidamente, ser�o eliminadas as guia_artigo_zona
    FOR l_guia_artigo_zona IN cur_guia_artigo_zona(p_id_guia_saida) LOOP
         UPDATE zona_fisica_artigo
         SET stock = zona_fisica_artigo.stock + l_guia_artigo_zona.quantidade
         WHERE zona_fisica_artigo.id_zona_fisica = l_guia_artigo_zona.id_zona_fisica;

        DBMS_OUTPUT.PUT_LINE('Stock Atualizado');
    END LOOP;

    DELETE FROM guia_artigo_zona WHERE id_guia_saida=p_id_guia_saida;
 END;
 /   

-- TESTES

/**
 * Cria guia_artigo_zona's da nota encomenda 1 para a guia saida 1 para que estas possam ser eliminadas no teste do procedimento
 * sem alterar os valores da base de dados.
 */
BEGIN
    proc_criar_guia_artigo_zona(1,'NE1');
END;

/**
 * Elimina todas as guia_artigo_zona da guia_saida com o c�digo 1 e atualiza os stocks das zonas fisicas correspondentes
 * O teste deve informar que o stock foi atualizado.
 */
BEGIN
    proc_eliminar_guia_artigo_zona(1);
END;
