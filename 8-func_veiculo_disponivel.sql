CREATE OR REPLACE FUNCTION func_veiculo_disponivel(p_matricula veiculo.matricula%TYPE)
RETURN BOOLEAN
IS
    l_cont_validos INTEGER;
BEGIN
    SELECT COUNT(*) INTO l_cont_validos
    FROM veiculo
    WHERE matricula = p_matricula
      AND disponivel LIKE '1';

    RETURN l_cont_validos != 0;
END;

-- Testes

SET SERVEROUTPUT ON;

/**
 * Teste em que � esperado que a fun��o retorne FALSE, pois o ve�culo n�o est� dispon�vel.
 */
DECLARE
    l_resultado BOOLEAN;
BEGIN
    l_resultado := func_veiculo_disponivel('12-FE-34');
    IF l_resultado THEN
        DBMS_OUTPUT.PUT_LINE('FALHOU');
    ELSE
        DBMS_OUTPUT.PUT_LINE('PASSOU');
    END IF;
END;

/**
 * Teste em que � esperado que a fun��o retorne TRUE, pois o ve�culo est� dispon�vel.
 */
DECLARE
    l_resultado BOOLEAN;
BEGIN
    l_resultado := func_veiculo_disponivel('14-GH-55');
    IF l_resultado THEN
        DBMS_OUTPUT.PUT_LINE('PASSOU');
    ELSE
        DBMS_OUTPUT.PUT_LINE('FALHOU');
    END IF;
END;
