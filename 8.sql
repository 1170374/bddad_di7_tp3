
/**
 * 8. Implementar um trigger designado trg_viagem_impedir_atribuicao_veiculo, que impeça que seja
 * atribuído um veículo a uma nova viagem, se este já efetuou o número máximo de quatro viagens
 * permitidas por semana ou, se está indisponível.
 */
CREATE OR REPLACE TRIGGER trg_viagem_impedir_atribuicao_veiculo
BEFORE INSERT ON viagem
FOR EACH ROW
BEGIN
    IF NOT func_veiculo_disponivel(:NEW.matricula) THEN
        RAISE_APPLICATION_ERROR(-20006, 'O veículo está indisponível.');
    END IF;
    IF func_contar_viagens(:NEW.matricula, :NEW.data_partida) >= 4 THEN
        RAISE_APPLICATION_ERROR(-20006, 'Um veiculo nao pode exceder 4 viagens feitas por semana.');
    END IF;
END;
/

-- TESTES

ALTER TRIGGER trg_viagem_impedir_atribuicao_veiculo ENABLE;

/**
 * Não é suposto esta inserção ser permitida.
 */
DECLARE
BEGIN
    INSERT INTO viagem VALUES (20, '23-GR-98', DATE '2018-12-01');
END;
/

/**
 * Esta inserção deve ser permitida.
 */
DECLARE
BEGIN
    INSERT INTO viagem VALUES (20, '23-GR-98', DATE '2014-12-01');
    DELETE FROM viagem WHERE num_viagem = 20;
END;
/

/**
 * Esta inserção não deve ser permitida, pois o veículo não está disponível.
 */
DECLARE
BEGIN
    INSERT INTO viagem VALUES (20, '12-FE-34', DATE '2014-12-01');
    DELETE FROM viagem WHERE num_viagem = 20;
END;
