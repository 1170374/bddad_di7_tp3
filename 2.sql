
SET SERVEROUTPUT ON;

/**
 * 2. Implementar uma função designada func_razao_veiculo_transporte, para obter a razão entre o
 * número de veículos que efetuaram viagens com o maior número de encomendas e o número de
 * veículos que efetuaram viagens com o menor número de encomendas. O cálculo desta razão deve
 * ser feito para um dado armazém num dado intervalo de tempo. A função tem como parâmetros, o
 * identificador do armazém e o período de tempo (data início e data fim). No caso de o identificador
 * do armazém não existir na BD, a função deve retornar o valor NULL.
*/
CREATE OR REPLACE FUNCTION func_razao_veiculo_transporte (p_inicio_periodo IN viagem.data_partida%TYPE,
                                                          p_fim_periodo    IN viagem.data_partida%TYPE,
                                                          p_cod_armazem    IN funcionario.cod_armazem%TYPE)
RETURN NUMBER
IS
    -- O valor mais alto e mais baixo de encomendas em todas as viagens respetivas ao periodo e armazem pretendido.
    l_max_encomendas          INTEGER;
    l_min_encomendas          INTEGER;

    -- A quantidade de veiculos que efetuaram alguma viagem com numero de encomendas igual ao mais alto ou mais baixo
    -- (mencionados acima).
    l_veiculos_max_encomendas INTEGER;
    l_veiculos_min_encomendas INTEGER;

    -- Todos os veiculos.
    CURSOR l_cur_veiculos IS
        SELECT *
        FROM veiculo;
    l_veiculo                 l_cur_veiculos%ROWTYPE;

    -- Todas as viagens correspondentes a um veiculo.
    CURSOR l_cur_viagens_veiculo(l_p_matricula viagem.matricula%TYPE) IS
        SELECT *
        FROM viagem VIA
        WHERE VIA.matricula = l_p_matricula
          AND VIA.data_partida BETWEEN p_inicio_periodo AND p_fim_periodo;
    l_viagem                  l_cur_viagens_veiculo%ROWTYPE;

    -- Contadores de viagens feitas pelo veiculo com quantidade de encomendas igual à mais alta ou mais baixa quantidade
    -- de encomendas.
    l_viagens_max_veiculo     INTEGER;
    l_viagens_min_veiculo     INTEGER;

    -- Numero de etapas/encomendas numa viagem (correspondentes a um armazem).
    l_etapas_viagem           INTEGER;

    -- Contador de armazens encontrados (no maximo é 1) para validar o parametro.
    l_cont_armazem            INTEGER;

    -- Exceções
    l_periodo_invalido        EXCEPTION;
    l_armazem_invalido        EXCEPTION;
    l_parametro_null          EXCEPTION;
    l_sem_encomendas          EXCEPTION;
BEGIN

    -- Verificar se os parametros nao sao null
    IF p_inicio_periodo IS NULL OR p_fim_periodo IS NULL OR p_cod_armazem IS NULL THEN
        RAISE l_parametro_null;
    END IF;

    IF p_fim_periodo < p_inicio_periodo THEN
        RAISE l_periodo_invalido;
    END IF;

    -- Verificar se o armazem é válido
    SELECT COUNT(*) INTO l_cont_armazem
    FROM armazem
    WHERE cod_armazem LIKE p_cod_armazem;

    IF l_cont_armazem = 0 THEN
        RAISE l_armazem_invalido;
    END IF;

    SELECT MAX(COUNT(*)), MIN(COUNT(*)) INTO l_max_encomendas, l_min_encomendas
    FROM viagem VIA INNER JOIN etapa_viagem    EV ON VIA.num_viagem       = EV.num_viagem
                    INNER JOIN guia_transporte GT ON EV.num_etapa         = GT.num_etapa AND EV.num_viagem = GT.num_viagem
                    INNER JOIN nota_encomenda  NE ON GT.id_nota_encomenda = NE.id_nota_encomenda
                    INNER JOIN funcionario     F  ON NE.nr_funcionario    = F.nr_funcionario
    WHERE VIA.data_partida BETWEEN p_inicio_periodo AND p_fim_periodo
      AND F.cod_armazem = p_cod_armazem
    GROUP BY VIA.num_viagem;

    IF l_max_encomendas = 0 OR l_min_encomendas = 0 THEN
        RAISE l_sem_encomendas;
    END IF;

    l_veiculos_max_encomendas := 0;
    l_veiculos_min_encomendas := 0;

    FOR l_veiculo IN l_cur_veiculos LOOP

        -- Contadores de viagens com o maximo numero de encomendas feitas pelo veiculo
        l_viagens_max_veiculo := 0;
        l_viagens_min_veiculo := 0;

        -- Iterar cada viagem para ver se tem o maximo numero de encomendas
        FOR l_viagem IN l_cur_viagens_veiculo(l_veiculo.matricula) LOOP

            -- Averiguar o numero de encomendas
            SELECT COUNT(*) INTO l_etapas_viagem
            FROM etapa_viagem EV INNER JOIN guia_transporte GT  ON EV.num_etapa         = GT.num_etapa AND EV.num_viagem = GT.num_viagem
                                 INNER JOIN nota_encomenda  NE  ON GT.id_nota_encomenda = NE.id_nota_encomenda
                                 INNER JOIN funcionario     F   ON NE.nr_funcionario    = F.nr_funcionario
            WHERE EV.num_viagem = l_viagem.num_viagem
              AND F.cod_armazem = p_cod_armazem;

            -- Se o numero de encomendas for igual ao numero mais alto ou mais baixo de encomendas, aumentar o
            -- contador correspondente.
            IF l_etapas_viagem = l_max_encomendas THEN
                l_viagens_max_veiculo := l_viagens_max_veiculo + 1;
            ELSE IF l_etapas_viagem = l_min_encomendas THEN
                l_viagens_min_veiculo := l_viagens_min_veiculo + 1;
            END IF;
            END IF;

            -- Se ja foram encontradas viagens com o numero mais baixo e mais alto de encomendas, mais iterações seriam
            -- redundantes. Logo, sair do loop interior
            IF l_viagens_max_veiculo > 0 AND l_viagens_min_veiculo > 0 THEN
                EXIT;
            END IF;
        END LOOP;

        -- Se foi encontrada alguma viagem com o mais alto ou mais baixo numero de encomendas, aumentar o contador
        -- respetivo.
        IF l_viagens_max_veiculo > 0 THEN
            l_veiculos_max_encomendas := l_veiculos_max_encomendas + 1;
        END IF;
        IF l_viagens_min_veiculo > 0 THEN
            l_veiculos_min_encomendas := l_veiculos_min_encomendas + 1;
        END IF;
    END LOOP;

    -- Calcular e retornar a razão
    RETURN l_veiculos_max_encomendas / l_veiculos_min_encomendas;
EXCEPTION
    WHEN l_periodo_invalido THEN
        DBMS_OUTPUT.PUT_LINE('O periodo e invalido.');
        RETURN NULL;
    WHEN l_armazem_invalido THEN
        DBMS_OUTPUT.PUT_LINE('O armazem e invalido.');
        RETURN NULL;
    WHEN l_parametro_null THEN
        DBMS_OUTPUT.PUT_LINE('Nenhum dos parametros pode ser null.');
        RETURN NULL;
    WHEN l_sem_encomendas THEN
        DBMS_OUTPUT.PUT_LINE('Não foram feitas encomendas neste intervalo');
        RETURN NULL;
END;
/

-- TESTES

/**
 * Procedimento para fazer cada teste.
 */
CREATE OR REPLACE PROCEDURE proc_testar_razao_veiculo_transporte(p_data_inicio IN viagem.data_partida%TYPE,
                                                                 p_data_fim    IN viagem.data_partida%TYPE,
                                                                 p_cod_armazem IN funcionario.cod_armazem%TYPE,
                                                                 p_esperado    IN NUMBER)
IS
    l_resultado       NUMBER;
    l_resultado_teste BOOLEAN;
BEGIN
    l_resultado := func_razao_veiculo_transporte(p_data_inicio, p_data_fim, p_cod_armazem);

    DBMS_OUTPUT.PUT_LINE('Teste com data inicial "' || p_data_inicio || '", data de fim "' || p_data_fim
                         || '" e codigo de armazem "' || p_cod_armazem || '".');
    DBMS_OUTPUT.PUT_LINE('Resultado: "' || l_resultado || '"');
    DBMS_OUTPUT.PUT_LINE('Esperado:  "' || p_esperado || '"');

    IF l_resultado IS NULL OR p_esperado IS NULL THEN
        l_resultado_teste := l_resultado IS NULL AND p_esperado IS NULL;
    ELSE
        l_resultado_teste := l_resultado = p_esperado;
    END IF;

    IF l_resultado_teste THEN
        DBMS_OUTPUT.PUT_LINE('Passou');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Falhou');
    END IF;
END;
/

/**
 * Teste para o intervalo de 01/01/2018 ate 01/12/2018 e armazem 1 onde o resultado esperado é 1.
 */
DECLARE
BEGIN
    proc_testar_razao_veiculo_transporte(TO_DATE('01/01/2018', 'DD/MM/YYYY'), TO_DATE('01/12/2018', 'DD/MM/YYYY'), 1, 1);
END;
/

/**
 * Teste para um intervalo em que não foram feitas encomendas, onde o resultado é NULL.
 */
DECLARE
BEGIN
    proc_testar_razao_veiculo_transporte(TO_DATE('01/12/2018', 'DD/MM/YYYY'), TO_DATE('01/12/2018', 'DD/MM/YYYY'), 1, NULL);
END;
/

/**
 * Teste para o primeiro parâmetro a NULL, onde o resultado é NULL.
 */
DECLARE
BEGIN
    proc_testar_razao_veiculo_transporte(NULL, TO_DATE('01/12/2018', 'DD/MM/YYYY'), 1, NULL);
END;
/

/**
 * Teste para o segundo parâmetro a NULL, onde o resultado é NULL.
 */
DECLARE
BEGIN
    proc_testar_razao_veiculo_transporte(TO_DATE('01/01/2018', 'DD/MM/YYYY'), NULL, 1, NULL);
END;
/

/**
 * Teste para o terceiro parâmetro a NULL, onde o resultado é NULL.
 */
DECLARE
BEGIN
    proc_testar_razao_veiculo_transporte(TO_DATE('01/01/2018', 'DD/MM/YYYY'), TO_DATE('01/12/2018', 'DD/MM/YYYY'), NULL, NULL);
END;
/

/**
 * Teste para o todos os parâmetros a NULL, onde o resultado é NULL.
 */
DECLARE
BEGIN
    proc_testar_razao_veiculo_transporte(NULL, NULL, NULL, NULL);
END;
/
