SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE proc_criar_guia_artigo_zona(p_id_guia_saida     IN  guia_saida.id_guia_saida%TYPE,
                                                        p_id_nota_encomenda IN  nota_encomenda.id_nota_encomenda%TYPE)
IS
    -- Cursor que cont�m todos oas artigo_encomenda da nota encomenda cujo id � recebido por par�metro
    CURSOR cur_artigo_encomenda(p_id_nota_encomenda IN nota_encomenda.id_nota_encomenda%TYPE) IS
        SELECT *
        FROM artigo_encomenda AE
        WHERE AE.id_nota_encomenda=p_id_nota_encomenda;

    -- Cursor que cont�m informa��o relativa a todas as zonas f�sicas existentes na base de dados
    CURSOR cur_zona_fisica IS
        SELECT *
        FROM zona_fisica_artigo;
            
    l_artigo_encomenda                   cur_artigo_encomenda%ROWTYPE;
    l_zona_fisica_artigo                 cur_zona_fisica%ROWTYPE;
    l_contador_nota_encomenda            INTEGER;
    l_stock                              INTEGER;
    l_stock_a_retirar                    INTEGER;

    l_excecao_nota_encomenda_inexistente EXCEPTION;
BEGIN

    SELECT COUNT(*) INTO l_contador_nota_encomenda
    FROM nota_encomenda NE
    WHERE NE.id_nota_encomenda = p_id_nota_encomenda;
    
    IF l_contador_nota_encomenda < 1 THEN
        RAISE l_excecao_nota_encomenda_inexistente;
    END IF;
    
    FOR l_artigo_encomenda IN cur_artigo_encomenda(p_id_nota_encomenda) LOOP
        l_stock := l_artigo_encomenda.quantidade;

        FOR l_zona_fisica_artigo IN cur_zona_fisica LOOP
            IF l_stock = 0 THEN
                EXIT;
            END IF;

            IF l_stock >= l_zona_fisica_artigo.stock THEN
                l_stock_a_retirar := l_zona_fisica_artigo.stock;
            ELSE
                l_stock_a_retirar := l_stock;
            END IF;

            -- Remover o stock do artigo da zona fisica.
            UPDATE zona_fisica_artigo
            SET stock = stock - l_stock_a_retirar
            WHERE id_zona_fisica    = l_zona_fisica_artigo.id_zona_fisica
              AND referencia_artigo = l_zona_fisica_artigo.referencia_artigo
              AND cod_armazem       = l_zona_fisica_artigo.cod_armazem;

            -- Atualizar o stock que ainda falta remover.
            l_stock := l_stock - l_stock_a_retirar;

            -- Criar um elemento da guia de saida.
            INSERT INTO guia_artigo_zona VALUES (p_id_guia_saida,
                                                 l_zona_fisica_artigo.id_zona_fisica,
                                                 l_zona_fisica_artigo.referencia_artigo,
                                                 l_zona_fisica_artigo.cod_armazem,
                                                 l_stock_a_retirar);


            DBMS_OUTPUT.PUT_LINE('Nova guia_artigo zona criada');
            DBMS_OUTPUT.PUT_LINE('ID guia saida: '|| p_id_guia_saida);
            DBMS_OUTPUT.PUT_LINE('ID zona fisica: '||l_zona_fisica_artigo.id_zona_fisica);
            DBMS_OUTPUT.PUT_LINE(l_zona_fisica_artigo.referencia_artigo);
            DBMS_OUTPUT.PUT_LINE('Armaz�m: '||l_zona_fisica_artigo.cod_armazem);
            DBMS_OUTPUT.PUT_LINE('Quantiadade: '||l_artigo_encomenda.quantidade);
        END LOOP;
    END LOOP;
        
EXCEPTION
    WHEN l_excecao_nota_encomenda_inexistente THEN
        DBMS_OUTPUT.PUT_LINE('ID da nota_encomenda inv�lido.');
 END;
 /   

-- Testes

/**
 * Teste que cria uma guia artigo zona com as seguintes informa��es:
 * ID guia saida: 1, ID zona fisica: Zona B2; Artigo 2; Armaz�m: 2; Quantiadade: 10
 */
DECLARE
BEGIN
    proc_criar_guia_artigo_zona(1,'NE1');
END;
/

/**
 * Elimina todas as guia_artigo_zona criados anteriormente para certificar que os valores do base de dados n�o s�o modificados.
 */
DECLARE
BEGIN
    proc_eliminar_guia_artigo_zona(1);
END;
/
