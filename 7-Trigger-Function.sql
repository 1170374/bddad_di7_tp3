SET SERVEROUTPUT ON;

/**
 * Fun��o que verifica se podem ser emitidas as faturas do cliente recebido por par�metro.
 * A fatura apenas � emitida se o cliente n�o tiver faturas por pagar h� mais de 3 meses.
 * A fun��o retorna 1 se for poss�vel inserir e 0 se n�o for poss�vel inserir a nota encomenda.
 */
CREATE OR REPLACE FUNCTION func_nota_encomenda_verificar_pagamentos(p_cod_cliente cliente.cod_cliente%TYPE)
RETURN BOOLEAN
IS
    -- Guarda a data de emiss�o das faturas que n�o foram pagas, de um determinado cliente.
    CURSOR l_cur_faturas_sem_pagar IS
        SELECT data_emissao
        FROM fatura F
        WHERE F.cod_cliente=p_cod_cliente
        AND (SELECT NVL(SUM(P.valor), 0)
               FROM pagamento P
               WHERE P.nr_fatura = F.nr_fatura) < F.valor_total;

    l_faturas_sem_pagar                    l_cur_faturas_sem_pagar%ROWTYPE;
    l_cod_cliente                          INTEGER;
    l_cod_cliente_inexistente              EXCEPTION;
BEGIN
    -- Guarda na vari�vel l_cod_cliente, a quantidade de clientes com o c�digo recebido por par�metro.
    SELECT COUNT(*) INTO l_cod_cliente
    FROM cliente C
    WHERE C.cod_cliente = p_cod_cliente;
    
    -- Se o c�digo do cliente n�o for encontrado na base de dados, � lan�ada a exce��o l_cod_cliente_inexistente.
    IF l_cod_cliente < 1 THEN
        RAISE l_cod_cliente_inexistente;
    END IF;
    
    FOR l_faturas_sem_pagar IN l_cur_faturas_sem_pagar LOOP
        -- Se o cliente tiver faturas por pagar h� mais de 3 meses, ent�o a fun��o retorna FALSE.
        IF MONTHS_BETWEEN(SYSDATE, l_faturas_sem_pagar.data_emissao) > 3 THEN
            RETURN FALSE;
        END IF;
    END LOOP;

    -- Se o cliente n�o tiver faturas por pagar h� mais de 3 meses, ent�o a fun��o retorna TRUE.
    RETURN TRUE;
    
EXCEPTION
    WHEN l_cod_cliente_inexistente THEN
        RETURN NULL;
END;
/  

-- TESTES

/**
 * Procedimento para fazer os v�rios testes.
 */
CREATE OR REPLACE PROCEDURE proc_testar_func_nota_encomenda_verificar_pagamentos(p_cod_cliente IN cliente.cod_cliente%TYPE,
                                                                                 p_esperado    IN BOOLEAN)
IS
    l_resultado       BOOLEAN;
    l_resultado_teste BOOLEAN;
BEGIN
    l_resultado := func_nota_encomenda_verificar_pagamentos(p_cod_cliente);

    DBMS_OUTPUT.PUT_LINE('Teste com c�digo de cliente "' || p_cod_cliente || '".');

    IF l_resultado IS NULL OR p_esperado IS NULL THEN
        l_resultado_teste := l_resultado IS NULL AND p_esperado IS NULL;
    ELSE
        l_resultado_teste := l_resultado = p_esperado;
    END IF;

    IF l_resultado_teste THEN
        DBMS_OUTPUT.PUT_LINE('Passou');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Falhou');
    END IF;
END;
/

/**
 * Testa a fun��o para um cliente com faturas por pagar h� mais de 3 meses.
 * Logo dever retornar FALSE, pois n�o � poss�vel adicionar a nota de encomenda.
 */
DECLARE
BEGIN
    proc_testar_func_nota_encomenda_verificar_pagamentos(1, FALSE);
END;
/

/**
 * Testa a fun��o para um cliente sem faturas por pagar h� mais de 3 meses.
 * Logo dever retornar TRUE, pois � poss�vel adicionar a nota de encomenda.
 */
DECLARE
BEGIN
    proc_testar_func_nota_encomenda_verificar_pagamentos(4, TRUE);
END;
/