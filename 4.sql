
SET SERVEROUTPUT ON;

/**
 * 4. Implementar um procedimento designado proc_faturas_nao_liquidadas, para realizar uma listagem
 * com as faturas, de um dado per�odo de tempo, que ainda n�o foram liquidadas. O procedimento tem
 * como par�metro o per�odo (data de in�cio e de fim) e no caso de n�o ser indicada uma das datas
 * ser�o analisadas todas as faturas n�o liquidadas. Deve ser apresentada informa��o (identificador,
 * data de fatura��o e valor total) das faturas em falta de cada cliente, ordenada pela data de fatura��o,
 * bem como, o subtotal a liquidar por cliente. De igual forma deve ser apresentado o total de todas as
 * faturas, de todos os que clientes, que faltam ser liquidadas � IsepBricolage.
 */
CREATE OR REPLACE PROCEDURE proc_faturas_nao_liquidadas (p_data_inicio IN fatura.data_emissao%TYPE DEFAULT NULL,
                                                         p_data_fim    IN fatura.data_emissao%TYPE DEFAULT NULL)
IS
    l_data_inicio          fatura.data_emissao%TYPE;
    l_data_fim             fatura.data_emissao%TYPE;

    l_contador             INTEGER;
    l_subtotal             fatura.valor_total%TYPE;
    l_subtotal_cliente     fatura.valor_total%TYPE;
    l_cod_cliente_anterior fatura.cod_cliente%TYPE;

    -- Obter as faturas por liquidar emitidas no periodo
    CURSOR l_cur_faturas (l_p_data_inicio viagem.data_partida%TYPE,
                          l_p_data_fim    viagem.data_partida%TYPE) IS
        SELECT *
        FROM fatura F
        WHERE F.data_emissao BETWEEN l_p_data_inicio AND l_p_data_fim

          AND (SELECT NVL(SUM(P.valor), 0)
               FROM pagamento P
               WHERE P.nr_fatura = F.nr_fatura) < F.valor_total
               ORDER BY F.cod_cliente, F.data_emissao;

    l_fatura               fatura%ROWTYPE;

    l_periodo_invalido     EXCEPTION;
    l_sem_faturas          EXCEPTION;
BEGIN

    -- Verificar se o periodo � invalido
    IF p_data_fim < p_data_inicio THEN
        RAISE l_periodo_invalido;
    END IF;

    -- Se um dos parametros for NULL, analisar todas as faturas
    IF p_data_inicio IS NULL OR p_data_fim IS NULL THEN
        l_data_inicio := TO_DATE('1/1/1', 'DD/MM/YYYY');
        l_data_fim := SYSDATE;
    ELSE
        l_data_inicio := p_data_inicio;
        l_data_fim := p_data_fim;
    END IF;

    -- Apresentar a informa��o das faturas n�o liquidadas
    DBMS_OUTPUT.PUT_LINE('Faturas n�o liquidadas:');

    -- A variavel l_cod_cliente_anterior ajuda a apresentar o codigo dos clientes quando ha varias faturas por liquidar
    -- correspondentes ao mesmo cliente.
    l_cod_cliente_anterior := null;
    l_subtotal_cliente := 0;

    FOR l_fatura IN  l_cur_faturas(l_data_inicio, l_data_fim) LOOP

        IF l_cod_cliente_anterior IS NOT NULL AND l_fatura.cod_cliente != l_cod_cliente_anterior THEN
            DBMS_OUTPUT.PUT_LINE('SUBTOTAL DO CLIENTE A LIQUIDAR: ' || l_subtotal_cliente);
            l_subtotal_cliente := 0;
        END IF;

        IF l_fatura.cod_cliente != l_cod_cliente_anterior OR l_cod_cliente_anterior IS NULL THEN
            DBMS_OUTPUT.PUT_LINE(' ');
            DBMS_OUTPUT.PUT_LINE('CLIENTE ' || l_fatura.cod_cliente || ':');
        END IF;

        -- Somar os pagamentos da fatura para calcular o subtotal.
        -- Se n�o forem encontrados pagamentos, considerar que falta o valor total.
        SELECT NVL(l_fatura.valor_total - SUM(P.valor), l_fatura.valor_total) INTO l_subtotal
        FROM pagamento P
        WHERE P.nr_fatura = l_fatura.nr_fatura;

        l_subtotal_cliente := l_subtotal_cliente + l_subtotal;

        DBMS_OUTPUT.PUT_LINE(RPAD('-', 13, '-'));
        DBMS_OUTPUT.PUT_LINE('Fatura ' || l_fatura.nr_fatura || ':');
        DBMS_OUTPUT.PUT_LINE(RPAD('-', 13, '-'));
        DBMS_OUTPUT.PUT_LINE(RPAD('Data de faturacao: ', 21) || TO_CHAR(l_fatura.data_emissao, 'DD/MM/YYYY'));
        DBMS_OUTPUT.PUT_LINE(RPAD('Valor total: ', 21) || l_fatura.valor_total);
        DBMS_OUTPUT.PUT_LINE(RPAD('Subtotal a liquidar: ', 21) || l_subtotal);

        l_cod_cliente_anterior := l_fatura.cod_cliente;
    END LOOP;

    -- Subtotal a liquidar do ultimo cliente
    IF l_subtotal_cliente != 0 THEN
        DBMS_OUTPUT.PUT_LINE('SUBTOTAL DO CLIENTE A LIQUIDAR: ' || l_subtotal_cliente);
    END IF;

EXCEPTION
    WHEN l_periodo_invalido THEN
        DBMS_OUTPUT.PUT_LINE('O periodo e invalido!');
    WHEN l_sem_faturas THEN
        DBMS_OUTPUT.PUT_LINE('Nao existem faturas por liquidar neste periodo.');
END;
/

-- TESTES

/**
 * Teste dado um intervalo.
 * � esperado o seguinte resultado:
 * Fatura 923 do cliente 1 com data de fatura�ao 15/02/2011, valor total 20000 e subtotal por liquidar 15000;
 * Fatura 950 do cliente 1 com data de fatura�ao 15/03/2011, valor total 15000 e subtotal por liquidar 15000;
 * Fatura 940 do cliente 2 com data de fatura�ao 15/03/2011, valor total 10000 e subtotal por liquidar 7000;
 * Fatura 1021 do cliente 2 com data de fatura�ao 15/03/2012, valor total 10000 e subtotal por liquidar 10000;
 */
DECLARE
BEGIN
    proc_faturas_nao_liquidadas(TO_DATE('01/01/2011', 'DD/MM/YYYY'), TO_DATE('01/04/2012', 'DD/MM/YYYY'));
END;
/

/**
 * Teste sem par�metros.
 * � esperado o mesmo resultado do teste anterior, juntamente com todas as outras faturas.
 */
DECLARE
BEGIN
    proc_faturas_nao_liquidadas();
END;
/

/**
 * Teste para o segundo par�metro sendo null.
 * � esperado o mesmo resultado do teste anterior.
 */
DECLARE
BEGIN
    proc_faturas_nao_liquidadas(TO_DATE('15/01/2019', 'DD/MM/YYYY'));
END;
/

/**
 * Teste para o primeiro par�metro sendo null.
 * � esperado o mesmo resultado do teste anterior.
 */
DECLARE
BEGIN
    proc_faturas_nao_liquidadas(NULL, TO_DATE('15/01/2019', 'DD/MM/YYYY'));
END;
/