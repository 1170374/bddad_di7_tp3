DROP TABLE zona_geografica           CASCADE CONSTRAINTS PURGE;
DROP TABLE armazem                   CASCADE CONSTRAINTS PURGE;
DROP TABLE zona_fisica               CASCADE CONSTRAINTS PURGE;
DROP TABLE artigo                    CASCADE CONSTRAINTS PURGE;
DROP TABLE zona_fisica_artigo        CASCADE CONSTRAINTS PURGE;
DROP TABLE historico_mudanca_preco   CASCADE CONSTRAINTS PURGE;
DROP TABLE stock_artigo              CASCADE CONSTRAINTS PURGE;
DROP TABLE funcionario               CASCADE CONSTRAINTS PURGE;
DROP TABLE categoria                 CASCADE CONSTRAINTS PURGE;
DROP TABLE fatura                    CASCADE CONSTRAINTS PURGE;
DROP TABLE linha_fatura              CASCADE CONSTRAINTS PURGE;
DROP TABLE pagamento                 CASCADE CONSTRAINTS PURGE;
DROP TABLE info_isepbricolage        CASCADE CONSTRAINTS PURGE;
DROP TABLE guia_saida                CASCADE CONSTRAINTS PURGE;
DROP TABLE transporte_mercadorias    CASCADE CONSTRAINTS PURGE;
DROP TABLE nota_encomenda            CASCADE CONSTRAINTS PURGE;
DROP TABLE estado                    CASCADE CONSTRAINTS PURGE;
DROP TABLE cliente                   CASCADE CONSTRAINTS PURGE;
DROP TABLE tipo_cliente              CASCADE CONSTRAINTS PURGE;
DROP TABLE artigo_encomenda          CASCADE CONSTRAINTS PURGE;
DROP TABLE guia_artigo_zona          CASCADE CONSTRAINTS PURGE;
DROP TABLE atualizacao_tipo_cliente  CASCADE CONSTRAINTS PURGE;
DROP TABLE modelo                    CASCADE CONSTRAINTS PURGE;
DROP TABLE marca                     CASCADE CONSTRAINTS PURGE;
DROP TABLE veiculo                   CASCADE CONSTRAINTS PURGE;
DROP TABLE viagem                    CASCADE CONSTRAINTS PURGE;
DROP TABLE etapa_viagem              CASCADE CONSTRAINTS PURGE;
DROP TABLE guia_transporte           CASCADE CONSTRAINTS PURGE;
DROP TABLE produtos_falta            CASCADE CONSTRAINTS PURGE;

CREATE TABLE zona_geografica (

    id_zona_geografica     VARCHAR(10),
    descricao              VARCHAR(20),

    CONSTRAINT pk_zona_geografica_id_zona_geografica PRIMARY KEY (id_zona_geografica)
);

CREATE TABLE armazem (

    cod_armazem            INTEGER,
    id_zona_geografica     VARCHAR(20)     CONSTRAINT nn_armazem_id_zona_geografica NOT NULL,
    nome                   VARCHAR(40)     CONSTRAINT nn_armazem_nome               NOT NULL,
    morada                 VARCHAR(40)     CONSTRAINT nn_armazem_morada             NOT NULL,
    latitude               FLOAT           CONSTRAINT nn_armazem_latitude           NOT NULL
                                           CONSTRAINT ck_armazem_coordenadas        CHECK(latitude >= 0 AND latitude <= 90),
    longitude              FLOAT           CONSTRAINT nn_armazem_longitude          NOT NULL
                                           CONSTRAINT ck_armazem_longitude          CHECK(longitude >= 0 AND longitude <= 180),

    CONSTRAINT pk_armazem_cod_armazem PRIMARY KEY (cod_armazem)
);

--Criar depois de Armazem
CREATE TABLE zona_fisica (

    id_zona_fisica         VARCHAR(10),
    cod_armazem            INTEGER,
    capacidade             INTEGER         CONSTRAINT nn_zona_fisica_capacidade NOT NULL,

    CONSTRAINT pk_zona_fisica_id_zona_fisica_cod_armazem PRIMARY KEY (id_zona_fisica, cod_armazem)
);

CREATE TABLE artigo (

    referencia_artigo      VARCHAR(10),
    nome                   VARCHAR(20)     CONSTRAINT nn_artigo_nome         NOT NULL,
    descricao              VARCHAR(30)     CONSTRAINT nn_artigo_descricao    NOT NULL,
    preco_compra           NUMBER(7,2)     CONSTRAINT nn_artigo_preco_compra NOT NULL,
    preco_venda            NUMBER(7,2)     CONSTRAINT nn_artigo_preco_venda  NOT NULL,
    unidade                VARCHAR(10)     CONSTRAINT nn_artigo_unidade      NOT NULL,
    peso                   NUMBER(7,2)     CONSTRAINT nn_artigo_peso         NOT NULL,
    volume                 NUMBER(7,2)     CONSTRAINT nn_artigo_volume       NOT NULL,

    CONSTRAINT  pk_artigo_referencia_artigo PRIMARY KEY (referencia_artigo)
);

--Criar depois de Armazem, Zona_Fisica e Artigo
CREATE TABLE zona_fisica_artigo (

    id_zona_fisica         VARCHAR(10),
    referencia_artigo      VARCHAR(10),
    cod_armazem            INTEGER,
    stock                  INTEGER         CONSTRAINT nn_zona_fisica_stock_nn NOT NULL,

    CONSTRAINT pk_zona_fisica_artigo_id_zona_fisica_referencia_artigo_cod_armazem PRIMARY KEY (id_zona_fisica, referencia_artigo, cod_armazem)
);

--Criar depois de Artigo
CREATE TABLE historico_mudanca_preco (

    data_inicio            DATE,
    referencia_artigo      VARCHAR(10),
    data_fim               DATE,
    preco_venda            NUMBER(7,2)      CONSTRAINT nn_historico_mudanca_preco_preco_venda NOT NULL,

    CONSTRAINT pk_historico_mudanca_preco_data_inicio_referencia_artigo PRIMARY KEY (data_inicio, referencia_artigo)
);


CREATE TABLE stock_artigo (

    cod_armazem            INTEGER,
    referencia_artigo      VARCHAR(10),
    stock_minimo           INTEGER         CONSTRAINT nn_stock_artigo_stock_minimo NOT NULL,

    CONSTRAINT pk_stock_artigo_cod_armazem_referencia_artigo PRIMARY KEY (cod_armazem, referencia_artigo)
);

--Criar tabela depois de zona geografica, armazem
CREATE TABLE funcionario (

    nr_funcionario         INTEGER,
    id_supervisor          INTEGER,
    cod_armazem            INTEGER         CONSTRAINT nn_funcionario_cod_armazem        NOT NULL,
    id_categoria           VARCHAR(10)     CONSTRAINT nn_funcionario_id_categoria       NOT NULL,
    morada                 VARCHAR(40)     CONSTRAINT nn_funcionario_morada             NOT NULL,
    salario_mensal         NUMBER(7,2)     CONSTRAINT nn_funcionario_salario_mensal     NOT NULL,
    cartao_cidadao         NUMBER(9,0)     CONSTRAINT nn_funcionario_nr_cc              NOT NULL,
    nome                   VARCHAR(40)     CONSTRAINT nn_funcionario_nome               NOT NULL,
    nif                    NUMBER(9,0)     CONSTRAINT nn_funcionario_nif                NOT NULL,
    
    CONSTRAINT pk_funcionario_nr_funcionario PRIMARY KEY (nr_funcionario)
);

CREATE TABLE categoria (
    id_categoria           VARCHAR(10),
    descricao              VARCHAR(20)     CONSTRAINT nn_categoria_descricao            NOT NULL,

    CONSTRAINT pk_categoria_id_categoria PRIMARY KEY (id_categoria)
);

CREATE TABLE fatura (

    nr_fatura              INTEGER,
    cod_cliente            INTEGER         CONSTRAINT nn_fatura_cod_cliente             NOT NULL,
    nr_funcionario         INTEGER         CONSTRAINT nn_fatura_nr_funcionario          NOT NULL,
    id_info                VARCHAR(10)     CONSTRAINT nn_fatura_id_info                 NOT NULL,
    data_emissao           TIMESTAMP       CONSTRAINT nn_fatura_data_emissao            NOT NULL
                                           CONSTRAINT ck_fatura_data_emissao            CHECK (EXTRACT(DAY FROM data_emissao) = 15),
    valor_total            NUMBER(10,2)    CONSTRAINT nn_fatura_valor_total             NOT NULL,
    
    CONSTRAINT pk_fatura_nr_fatura PRIMARY KEY (nr_fatura)
);

CREATE TABLE linha_fatura (
    
    nr_fatura             INTEGER,
    referencia_artigo     VARCHAR(10)     CONSTRAINT nn_linha_fatura_referencia_artigo  NOT NULL,
    quantidade            INTEGER         CONSTRAINT nn_linha_fatura_quantidade         NOT NULL,
    preco_final           NUMBER(7,2)     CONSTRAINT nn_linha_fatura_preco_final        NOT NULL,
    iva                   INTEGER         CONSTRAINT nn_linha_fatura_iva                NOT NULL
                                          CONSTRAINT ck_linha_fatura_iva                CHECK (iva BETWEEN 0 AND 100),
    desconto              INTEGER     	  CONSTRAINT nn_linha_fatura_desconto           NOT NULL
                                          CONSTRAINT ck_linha_fatura_desconto           CHECK (desconto BETWEEN 0 AND 100),
                                            
    CONSTRAINT pk_linha_fatura_nr_fatura PRIMARY KEY (nr_fatura)
                                            
);

CREATE TABLE pagamento (
    nr_fatura           INTEGER,
    valor               NUMBER(10,2)    CONSTRAINT nn_pagamento_valor                   NOT NULL,
    
    CONSTRAINT pk_pagamento_nr_fatura PRIMARY KEY (nr_fatura)
);
  
CREATE TABLE info_isepbricolage (
    id_info             VARCHAR(10),
    nome_empresa        VARCHAR(20)     CONSTRAINT nn_info_isepbricolage_nome_empresa   NOT NULL,
    morada_sede         VARCHAR(40)     CONSTRAINT nn_info_isepbricolage_morada_sede    NOT NULL,
    codigo_postal       NUMBER(7,0)     CONSTRAINT nn_info_isepbricolage_codigo_postal  NOT NULL,
    localidade          VARCHAR(20)     CONSTRAINT nn_info_isepbricolage_localidade     NOT NULL,
    telefone            NUMBER(9,0)     CONSTRAINT nn_info_isepbricolage_telefone       NOT NULL,
    fax                 NUMBER(8,0)     CONSTRAINT nn_info_isepbricolage_fax            NOT NULL,
    nif                 NUMBER(9,0)     CONSTRAINT nn_info_isepbricolage_nif            NOT NULL,

    CONSTRAINT pk_info_isepbricolage_id_info PRIMARY KEY (id_info)
);    

CREATE TABLE guia_saida (

    id_guia_saida       INTEGER GENERATED AS IDENTITY,
    id_nota_encomenda   VARCHAR(10),

    CONSTRAINT pk_guia_saida_id_guia_saida PRIMARY KEY (id_guia_saida)
);

CREATE TABLE transporte_mercadorias (

    id_zona_geografica   VARCHAR(10),
    nr_funcionario       INTEGER,
    distancia            INTEGER        CONSTRAINT nn_transporte_mercadorias_distancia NOT NULL
                                        CONSTRAINT ck_transporte_mercadorias_distancia CHECK (distancia <= 200),

    CONSTRAINT pk_transporte_mercadorias_id_zona_geografica_nr_funcionario PRIMARY KEY (id_zona_geografica, nr_funcionario)
);

CREATE TABLE nota_encomenda (

    id_nota_encomenda      VARCHAR(10),
    nr_funcionario         INTEGER         CONSTRAINT nn_nota_encomenda_nr_funcionario NOT NULL,
    cod_cliente            INTEGER         CONSTRAINT nn_nota_encomenda_cod_cliente    NOT NULL,
    id_estado              VARCHAR(10)     CONSTRAINT nn_nota_encomenda_id_estado      NOT NULL,
    semana                 DATE            CONSTRAINT nn_nota_encomenda_semana         NOT NULL,

    CONSTRAINT pk_nota_encomenda_id_nota_encomenda PRIMARY KEY (id_nota_encomenda)
);

CREATE TABLE estado (

    id_estado              VARCHAR(10),
    descricao              VARCHAR(20)    CONSTRAINT nn_estado_descricao               NOT NULL,

    CONSTRAINT pk_estado_id_estado PRIMARY KEY (id_estado)
);

CREATE TABLE cliente (

    cod_cliente            INTEGER,
    id_zona_geografica     VARCHAR(10)     CONSTRAINT nn_cliente_id_zona_geografica NOT NULL,
    id_tipo_cliente        VARCHAR(10)     CONSTRAINT nn_cliente_id_tipo_cliente    NOT NULL,
    nome                   VARCHAR(40)     CONSTRAINT nn_cliente_nome               NOT NULL,
    morada                 VARCHAR(40)     CONSTRAINT nn_cliente_morada             NOT NULL,
    codigo_postal          VARCHAR(7)      CONSTRAINT nn_cliente_codigo_postal      NOT NULL,
    telemovel              VARCHAR(9)      CONSTRAINT nn_cliente_telemovel          NOT NULL,
    nif                    VARCHAR(9)      CONSTRAINT nn_cliente_nif                NOT NULL,
    volume_total           NUMBER(7,2)     CONSTRAINT nn_cliente_volume_total       NOT NULL,

    CONSTRAINT pk_cliente_cod_cliente PRIMARY KEY (cod_cliente)
);

CREATE TABLE tipo_cliente (

    id_tipo_cliente         VARCHAR(10),
    descricao               VARCHAR(20)    CONSTRAINT nn_tipo_cliente_descricao          NOT NULL,
    volume_necessario       NUMBER(7,2)    CONSTRAINT nn_tipo_cliente_volume_necessario  NOT NULL,

    CONSTRAINT pk_tipo_id_tipo_cliente PRIMARY KEY (id_tipo_cliente)
);

CREATE TABLE artigo_encomenda (

    referencia_artigo      VARCHAR(10),
    id_nota_encomenda      VARCHAR(10),
    quantidade             INTEGER        CONSTRAINT nn_artigo_encomenda_quantidade NOT NULL
                                          CONSTRAINT ck_artigo_encomenda_quantidade CHECK(quantidade > 0),

    CONSTRAINT pk_artigo_encomenda_referencia_id_nota_encomenda PRIMARY KEY (referencia_artigo, id_nota_encomenda)
);

CREATE TABLE guia_artigo_zona (
    id_guia_saida         INTEGER,
    id_zona_fisica        VARCHAR(10),
    referencia_artigo     VARCHAR(10),
    cod_armazem           INTEGER,
    quantidade            INTEGER         CONSTRAINT nn_guia_artigo_zona_quantidade NOT NULL
                                          CONSTRAINT ck_guia_artigo_zona_quantidade CHECK(quantidade > 0),
    
    CONSTRAINT pk_guia_artigo_zona_id_guia_saida_id_zona_fisica_referencia_artigo_cod_armazem PRIMARY KEY (id_guia_saida, id_zona_fisica, referencia_artigo, cod_armazem)
);

CREATE TABLE atualizacao_tipo_cliente (
    data_atualizacao    DATE,
    cod_cliente         INTEGER,
    id_tipo_cliente     VARCHAR(10)       CONSTRAINT nn_atualizacao_tipo_cliente    NOT NULL,
    
    CONSTRAINT pk_atualizacao_tipo_cliente_data_atualizacao_cod_cliente PRIMARY KEY (data_atualizacao, cod_cliente) 
);
 
CREATE TABLE modelo (

    id_modelo            VARCHAR(15)       CONSTRAINT pk_modelo_id_modelo           PRIMARY KEY,
    id_marca             VARCHAR(15)       CONSTRAINT nn_modelo_id_marca            NOT NULL,
    tipo_veiculo         VARCHAR(15)       CONSTRAINT nn_modelo_tipo_veiculo        NOT NULL,
    capacidade_volume    NUMBER            CONSTRAINT nn_modelo_capacidade_v        NOT NULL
                                           CONSTRAINT ck_modelo_capacidade_v        CHECK (capacidade_volume > 0),

    capacidade_peso      NUMBER            CONSTRAINT nn_modelo_capacidade_p        NOT NULL
                                           CONSTRAINT ck_modelo_capacidade_p        CHECK (capacidade_peso > 0)
);

CREATE TABLE marca (
    id_marca            VARCHAR(15),    
    nome                VARCHAR(15)        CONSTRAINT nn_marca_nome                 NOT NULL,
    contacto            NUMBER(9,0)        CONSTRAINT nn_marca_contacto             NOT NULL,
    
    CONSTRAINT pk_marca_id_marca PRIMARY KEY (id_marca)
);

CREATE TABLE veiculo (

    matricula              VARCHAR(8)      CONSTRAINT pk_veiculo_matricula   PRIMARY KEY,
    id_modelo              VARCHAR(15)     CONSTRAINT nn_veiculo_id_modelo   NOT NULL,
    num_seguro             INTEGER,
    cont_km                NUMBER(8,1)     CONSTRAINT nn_veiculo_cont_km     NOT NULL,
    disponivel             CHAR(1)         CONSTRAINT nn_veiculo_disponivel  NOT NULL -- CHAR(1) funciona como valor booleano
);

CREATE TABLE viagem (

    num_viagem             INTEGER         CONSTRAINT pk_viagem_num_viagem PRIMARY KEY,
    matricula              VARCHAR(8),
    data_partida           DATE
);

CREATE TABLE etapa_viagem (

    num_etapa              INTEGER,
    num_viagem             INTEGER,

    CONSTRAINT pk_etapa_viagem PRIMARY KEY (num_etapa, num_viagem)
);

CREATE TABLE guia_transporte (

    id_nota_encomenda      VARCHAR(10),
    num_viagem             INTEGER,
    num_etapa              INTEGER,
    
    CONSTRAINT pk_guia_transporte PRIMARY KEY (id_nota_encomenda, num_viagem, num_etapa)
);

CREATE TABLE produtos_falta (

    referencia_artigo      VARCHAR(10),
    id_nota_encomenda      VARCHAR(10),
    quantidade_falta       INTEGER        CONSTRAINT nn_produtos_falta_quantidade NOT NULL
                                          CONSTRAINT ck_produtos_falta_quantidade CHECK(quantidade_falta > 0),

    CONSTRAINT pk_produtos_falta_referencia_artigo_id_nota_encomenda PRIMARY KEY (referencia_artigo, id_nota_encomenda)
);
    

--Chaves Estrangeiras

--Tabela zona_fisica
ALTER TABLE zona_fisica             ADD CONSTRAINT fk_zona_fisica_cod_armazem                       FOREIGN KEY (cod_armazem)                   REFERENCES armazem(cod_armazem);

--Tabela zona_fisica_artigo
ALTER TABLE zona_fisica_artigo      ADD CONSTRAINT fk_zona_fisica_artigo_id_zona_fisica_cod_armazem FOREIGN KEY (id_zona_fisica,cod_armazem)    REFERENCES zona_fisica(id_zona_fisica,cod_armazem);
ALTER TABLE zona_fisica_artigo      ADD CONSTRAINT fk_zona_fisica_artigo_referencia_artigo          FOREIGN KEY (referencia_artigo)             REFERENCES artigo(referencia_artigo);

--Tabela historico_mudanca_preco
ALTER TABLE historico_mudanca_preco ADD CONSTRAINT fk_historico_mudanca_preco_referencia_artigo     FOREIGN KEY (referencia_artigo)             REFERENCES artigo(referencia_artigo);

--Table stock_artigo
ALTER TABLE stock_artigo            ADD CONSTRAINT fk_stock_artigo_cod_armazem                      FOREIGN KEY (cod_armazem)                   REFERENCES armazem(cod_armazem);
ALTER TABLE stock_artigo            ADD CONSTRAINT fk_stock_artigo_referencia_artigo                FOREIGN KEY (referencia_artigo)             REFERENCES artigo(referencia_artigo);

--Tabela funcionario
ALTER TABLE funcionario             ADD CONSTRAINT fk_funcionario_id_supervisor                     FOREIGN KEY (id_supervisor)                 REFERENCES funcionario(nr_funcionario);
ALTER TABLE funcionario             ADD CONSTRAINT fk_funcionario_cod_armazem                       FOREIGN KEY (cod_armazem)                   REFERENCES armazem(cod_armazem);
ALTER TABLE funcionario             ADD CONSTRAINT fk_funcionario_id_categoria                      FOREIGN KEY (id_categoria)                  REFERENCES categoria(id_categoria);

--Tabela armazem
ALTER TABLE armazem                 ADD CONSTRAINT fk_armazem_id_zona_geografica                    FOREIGN KEY (id_zona_geografica)            REFERENCES zona_geografica(id_zona_geografica);

--Tabela fatura
ALTER TABLE fatura                  ADD CONSTRAINT fk_fatura_cod_cliente                            FOREIGN KEY (cod_cliente)                   REFERENCES cliente(cod_cliente);
ALTER TABLE fatura                  ADD CONSTRAINT fk_fatura_nr_funcionario                         FOREIGN KEY (nr_funcionario)                REFERENCES funcionario(nr_funcionario);
ALTER TABLE fatura                  ADD CONSTRAINT fk_fatura_id_info                                FOREIGN KEY (id_info)                       REFERENCES info_isepbricolage(id_info);

--Tabela linha_fatura
ALTER TABLE linha_fatura            ADD CONSTRAINT fk_linha_fatura_nr_fatura                        FOREIGN KEY (nr_fatura)                     REFERENCES fatura(nr_fatura);
ALTER TABLE linha_fatura            ADD CONSTRAINT fk_linha_fatura_referencia_artigo                FOREIGN KEY (referencia_artigo)             REFERENCES artigo(referencia_artigo);

--Tabela pagamento
ALTER TABLE pagamento               ADD CONSTRAINT fk_pagamento_nr_fatura                           FOREIGN KEY (nr_fatura)                     REFERENCES fatura(nr_fatura);

--Tabela transporte_mercadorias
ALTER TABLE transporte_mercadorias  ADD CONSTRAINT fk_transporte_mercadorias_id_zona_geografica     FOREIGN KEY (id_zona_geografica)            REFERENCES zona_geografica(id_zona_geografica);
ALTER TABLE transporte_mercadorias  ADD CONSTRAINT fk_transporte_mercadorias_nr_funcionario         FOREIGN KEY (nr_funcionario)                REFERENCES funcionario(nr_funcionario);

--Tabela nota_encomenda
ALTER TABLE nota_encomenda          ADD CONSTRAINT fk_nota_encomenda_nr_funcionario                 FOREIGN KEY (nr_funcionario)                REFERENCES funcionario(nr_funcionario);
ALTER TABLE nota_encomenda          ADD CONSTRAINT fk_nota_encomenda_cod_cliente                    FOREIGN KEY (cod_cliente)                   REFERENCES cliente(cod_cliente);
ALTER TABLE nota_encomenda          ADD CONSTRAINT fk_nota_encomenda_id_estado                      FOREIGN KEY (id_estado)                     REFERENCES estado(id_estado);

--Tabela cliente
ALTER TABLE cliente                 ADD CONSTRAINT fk_cliente_id_zona_geografica                    FOREIGN KEY (id_zona_geografica)            REFERENCES zona_geografica(id_zona_geografica);
ALTER TABLE cliente                 ADD CONSTRAINT fk_cliente_id_tipo_cliente                       FOREIGN KEY (id_tipo_cliente)               REFERENCES tipo_cliente(id_tipo_cliente);

--Tabela artigo_encomenda
ALTER TABLE artigo_encomenda        ADD CONSTRAINT fk_artigo_encomenda_referencia_artigo            FOREIGN KEY (referencia_artigo)             REFERENCES artigo(referencia_artigo);
ALTER TABLE artigo_encomenda        ADD CONSTRAINT fk_artigo_encomenda_id_nota_encomenda            FOREIGN KEY (id_nota_encomenda)             REFERENCES nota_encomenda(id_nota_encomenda);

-- Tabela modelo
ALTER TABLE modelo                  ADD CONSTRAINT fk_modelo_marca                                  FOREIGN KEY (id_marca)                      REFERENCES marca(id_marca);

-- Tabela veiculo
ALTER TABLE veiculo                 ADD CONSTRAINT fk_veiculo_id_modelo                             FOREIGN KEY (id_modelo)                     REFERENCES modelo(id_modelo);

-- Tabela viagem
ALTER TABLE viagem                  ADD CONSTRAINT fk_viagem_matricula                              FOREIGN KEY (matricula)                     REFERENCES veiculo(matricula);

-- Tabela etapa_viagem
ALTER TABLE etapa_viagem            ADD CONSTRAINT fk_etapa_viagem_num_viagem                       FOREIGN KEY (num_viagem)                    REFERENCES viagem(num_viagem);

-- Tabela guia_artigo_zona
ALTER TABLE guia_artigo_zona        ADD CONSTRAINT fk_guia_artigo_zona_id_zf_ref_art_cod_armazem    FOREIGN KEY (id_zona_fisica, referencia_artigo, cod_armazem)
                                                                                                                                                REFERENCES zona_fisica_artigo(id_zona_fisica, referencia_artigo, cod_armazem);
ALTER TABLE guia_artigo_zona        ADD CONSTRAINT fk_guia_artigo_zona_id_guia_saida                 FOREIGN KEY (id_guia_saida)                 REFERENCES guia_saida(id_guia_saida);

-- Tabela guia_saida
ALTER TABLE guia_saida              ADD CONSTRAINT fk_guia_artigo_id_nota_encomenda                 FOREIGN KEY (id_nota_encomenda)             REFERENCES nota_encomenda(id_nota_encomenda);

-- Tabela guia_transporte
ALTER TABLE guia_transporte         ADD CONSTRAINT fk_guia_transporte_id_nota_encomenda             FOREIGN KEY (id_nota_encomenda)             REFERENCES nota_encomenda(id_nota_encomenda);
ALTER TABLE guia_transporte         ADD CONSTRAINT fk_guia_transporte_num_viagem_etapa              FOREIGN KEY (num_viagem, num_etapa)         REFERENCES etapa_viagem(num_viagem, num_etapa);

ALTER TABLE produtos_falta          ADD CONSTRAINT fk_produtos_falta_referencia_artigo            FOREIGN KEY (referencia_artigo)             REFERENCES artigo(referencia_artigo);
ALTER TABLE produtos_falta          ADD CONSTRAINT fk_produtos_falta_id_nota_encomenda            FOREIGN KEY (id_nota_encomenda)             REFERENCES nota_encomenda(id_nota_encomenda);
