
SET SERVEROUTPUT ON;

/*
 * 6. Elaborar um  procedimento designado proc_atualizar_tipo_cliente, que atualize o valor do tipo de cliente, para todos os
 * clientes,de acordo com o volume de neg�cio que efetuaram (ver Tabela 1). O per�odo de volume de  neg�cios a ser considerado
 * � o dos 12 meses anteriores. No entanto, s� os clientes que foram atualizados h� mais de 12 meses � que podem ser atualizados.
 * Se se entender necess�rio podem ser realizadas altera��es ao modelo relacional.
 */
CREATE OR REPLACE PROCEDURE proc_atualizar_tipo_cliente
IS

	-- Cursor que contem todos os clientes cuja atualiza��o do tipo foi efetuda h� mais de 12 meses.
    CURSOR cur_clientes_atualizados_12_meses IS
        SELECT DISTINCT A.cod_cliente
        FROM atualizacao_tipo_cliente A
        WHERE A.cod_cliente NOT IN (SELECT A.cod_cliente
                                    FROM atualizacao_tipo_cliente A
                                    WHERE data_atualizacao > (ADD_MONTHS(SYSDATE,(-12))));

    -- Cursor que cont�m informa��o relativa a todos os tipos de clientes
    CURSOR cur_tipo_cliente IS
        SELECT *
        FROM tipo_cliente
        ORDER BY volume_necessario DESC;
            
    -- Cursor que cont�m informa��o relativa a todas as faturas referentes ao cliente cujo c�digo � passado por par�metro
    CURSOR cur_fatura_cliente(p_cod_cliente cliente.cod_cliente%TYPE) IS
        SELECT *
        FROM fatura F
        WHERE F.cod_cliente=p_cod_cliente
          AND F.data_emissao > (ADD_MONTHS(SYSDATE,(-12)));
            
    l_fatura_cliente                     cur_fatura_cliente%ROWTYPE;        
    l_tipo_cliente                       cur_tipo_cliente%ROWTYPE;
	l_cliente		        	         cur_clientes_atualizados_12_meses%ROWTYPE;
	l_volume_total			             INTEGER;
    
BEGIN
    
    -- Percorre todos os clientes cujo tipo foi atualizado h� mais de 12 meses
	FOR l_cliente IN cur_clientes_atualizados_12_meses
	LOOP
        -- Inicializa o contador de volume a 0
        l_volume_total:=0;          
        DBMS_OUTPUT.PUT_LINE('Cliente: '||l_cliente.cod_cliente);
        
        -- Percorre todas as faturas emitidas h� menos de 12 meses da data atual do cliente do loop exterior
		FOR l_fatura_cliente IN cur_fatura_cliente(l_cliente.cod_cliente)
		LOOP 
            -- Soma o valor de todas as faturas do loop
            l_volume_total := l_volume_total + l_fatura_cliente.valor_total;       
		END LOOP;
        DBMS_OUTPUT.PUT_LINE('Volume Total: '||l_volume_total);
           
        -- Percorre todos os tipos de cliente, caso o volume total calculado anteriormente seja superiro ao volume necess�rio para
        -- ser do tipo de cliente correspondente, atualiza-se o tipo de cliente e regista-se a nova atualiza��o na tabela atualizacao_tipo_cliente
		FOR l_tipo_cliente IN cur_tipo_cliente LOOP
            IF l_volume_total > l_tipo_cliente.volume_necessario THEN
                UPDATE cliente SET id_tipo_cliente = l_tipo_cliente.id_tipo_cliente WHERE cliente.cod_cliente = l_cliente.cod_cliente;
                INSERT INTO atualizacao_tipo_cliente VALUES (SYSDATE, l_cliente.cod_cliente, l_tipo_cliente.id_tipo_cliente);

                DBMS_OUTPUT.PUT_LINE('Tipo Cliente: '||l_tipo_cliente.id_tipo_cliente);

                EXIT;
            END IF;
        END LOOP;
	END LOOP;
END;
/

-- TESTES

/**
 * Teste do procedimento, cliente 1 e 2 s�o despromovidos a pequenos clientes e cliente 3 � promovido a grande cliente
 */
DECLARE
BEGIN
    proc_atualizar_tipo_cliente();

    -- Devem ser geradas as tabelas e dados correspondentes depois de correr este teste, devido � impossibilidade de
    -- restaurar todos os dados.
END;
/

/**
 * Correndo o teste uma segunda vez n�o ir� atualizar nenhum cliente pois o teste anterior atualizou todos os clientes poss�veis,
 * n�o existindo nenhum cliente por atualizar h� mais de 12 meses.
 */
DECLARE
BEGIN
    proc_atualizar_tipo_cliente();
    proc_atualizar_tipo_cliente();

    -- Devem ser geradas as tabelas e dados correspondentes depois de correr este teste, devido � impossibilidade de
    -- restaurar todos os dados.
END;
/
