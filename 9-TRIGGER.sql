
SET SERVEROUTPUT ON;

/**
  * 9. Implementar um trigger designado trg_guia_saida_atualizar_stock, que garanta que o stock � corretamente atualizado de 
  * acordo com a informa��o das guias de sa�da,ou seja,quando se cria,altera ou apaga uma linha de uma determinada guia de sa�da.
  */
CREATE OR REPLACE TRIGGER trg_guia_saida_atualizar_stock
AFTER INSERT OR DELETE OR UPDATE ON guia_saida
FOR EACH ROW
DECLARE	
BEGIN
	 IF INSERTING THEN
        proc_criar_guia_artigo_zona(:NEW.id_guia_saida, :NEW.id_nota_encomenda);
     END IF;

     IF DELETING THEN
		proc_eliminar_guia_artigo_zona(:OLD.id_guia_saida);
     END IF;

     IF UPDATING  THEN     
        proc_eliminar_guia_artigo_zona(:OLD.id_guia_saida);
        proc_criar_guia_artigo_zona(:NEW.id_guia_saida, :NEW.id_nota_encomenda);
     END IF;
END;
/

-- TESTES

ALTER TRIGGER trg_guia_saida_atualizar_stock ENABLE;

--Teste 1: Ativa o trigger quando � inserida uma guia_saida na base de dados
INSERT INTO guia_saida (id_nota_encomenda) VALUES('NE8');
--Teste 2: Ativa o trigger quando � eliminada uma guia_saida na base de dados
DELETE FROM guia_saida WHERE id_nota_encomenda='NE8';
--Teste 3: Ativa o trigger quando s�o efetuadas altera��es na guia_saida
UPDATE guia_saida SET id_nota_encomenda='NE8' WHERE id_nota_encomenda='NE1';
UPDATE guia_saida SET id_nota_encomenda='NE1' WHERE id_nota_encomenda='NE8';

