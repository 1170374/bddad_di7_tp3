
SET SERVEROUTPUT ON;

/**
 * 1. Implementar uma fun��o designada func_stock_artigo_armazem, para retornar a quantidade total
 * de um artigo num determinado armaz�m. A fun��o tem como par�metros, o identificador do artigo
 * e o identificador do armaz�m. No caso de o identificador do artigo ou do armaz�m n�o existir na BD,
 * a fun��o deve retornar o valor NULL.
 */
CREATE OR REPLACE FUNCTION func_stock_artigo_armazem(p_cod_armazem       IN armazem.cod_armazem%TYPE,
                                                     p_referencia_artigo IN artigo.referencia_artigo%TYPE)
RETURN INTEGER
IS
    -- Cursor que contem todas as zonas fisicas do armazem cujo c�digo � passado por par�metro
    -- e que cont�m o artigo cuja referencia � passada por par�metro.
   CURSOR cur_zona_fisica(p_cod_armazem armazem.cod_armazem%TYPE, p_referencia_artigo artigo.referencia_artigo%TYPE) IS
         SELECT  *
         FROM zona_fisica_artigo ZFA
         WHERE ZFA.cod_armazem = p_cod_armazem
           AND ZFA.referencia_artigo = p_referencia_artigo;

   l_zona_fisica_artigo        zona_fisica_artigo%ROWTYPE;
   l_contador_armazem          INTEGER;
   l_contador_artigo           INTEGER;
   l_stock_artigo              INTEGER;

   l_parametro_invalido        EXCEPTION;
    
BEGIN

    -- Guarda na vari�vel l_contador_armazem a quantidade de armazens com o c�digo recebido por par�metro.
    SELECT COUNT(*) INTO l_contador_armazem
    FROM armazem A
    WHERE A.cod_armazem = p_cod_armazem;
    
    -- Guarda na vari�vel l_contador_artigo a quantidade de artigos com o c�digo recebido por par�metro.
    SELECT COUNT(*) INTO l_contador_artigo
    FROM artigo AR
    WHERE AR.referencia_artigo = p_referencia_artigo;

    l_stock_artigo := 0;

    -- Caso o armazem ou o artigo recebidos por par�metro n�o sejam encontrados na base de dados,
    -- � lan�ada a exce��o l_parametro_invalido.
    IF (l_contador_armazem < 1 OR l_contador_artigo < 1) THEN
        RAISE l_parametro_invalido;
    ELSE
        -- Caso contr�rio, ser� somado o stock de todas as zonas_fisicas_artigos do cursor cur_zona_fisica
        -- e retornado o valor total.
        FOR l_zona_fisica_artigo IN cur_zona_fisica(p_cod_armazem,p_referencia_artigo) LOOP
            l_stock_artigo := l_stock_artigo + l_zona_fisica_artigo.stock;
        END LOOP;

        RETURN l_stock_artigo;
    END IF;
EXCEPTION
    -- Caso a exce��o l_parametro_invalido seja lan�ada � retornado NULL.
    WHEN l_parametro_invalido THEN
         RETURN NULL;
END;
/

-- TESTES

/**
 * Procedimento para fazer os varios testes.
 */
CREATE OR REPLACE PROCEDURE proc_testar_stock_artigo_armazem(p_cod_armazem       IN armazem.cod_armazem%TYPE,
                                                             p_referencia_artigo IN artigo.referencia_artigo%TYPE,
                                                             p_esperado          IN NUMBER)
IS
    l_resultado       NUMBER;
    l_resultado_teste BOOLEAN;
BEGIN
    l_resultado := func_stock_artigo_armazem(p_cod_armazem, p_referencia_artigo);

    DBMS_OUTPUT.PUT_LINE('Teste com c�digo armazem "' || p_cod_armazem || '" e refer�ncia artigo "' || p_referencia_artigo);
    DBMS_OUTPUT.PUT_LINE('Resultado: ' || l_resultado);
    DBMS_OUTPUT.PUT_LINE('Esperado:  ' || p_esperado);

    IF l_resultado IS NULL OR p_esperado IS NULL THEN
        l_resultado_teste := l_resultado IS NULL AND p_esperado IS NULL;
    ELSE
        l_resultado_teste := l_resultado = p_esperado;
    END IF;

    IF l_resultado_teste THEN
        DBMS_OUTPUT.PUT_LINE('Passou');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Falhou');
    END IF;
END;
/

/**
 * Teste que retorna a quantidade de stock do Artigo 4 que se encontra apenas numa zona fisica do armaz�m de c�digo 2.
 * Deve retornar 156
 */
DECLARE
BEGIN
    proc_testar_stock_artigo_armazem(2, 'Artigo 4', 156);
END;
/

/**
 * Teste que retorna a quantidade de stock do Artigo 1 que se encontra em v�rias zonas fisicas do armaz�m de c�digo 2.
 * Deve retornar 35
 */
DECLARE
BEGIN
    proc_testar_stock_artigo_armazem(2, 'Artigo 1', 35);
END;
/

/**
 * Teste que retorna a quantidade de stock do Artigo 4 no armaz�m de c�digo 5.
 * Como o artigo n�o est� presente no armaz�a fun��o deve retornar 0.
 */
DECLARE
BEGIN
    proc_testar_stock_artigo_armazem(5, 'Artigo 4', 0);
END;
/

/**
 * Teste que retorna a quantidade de stock do Artigo 4 num armazem inexistente.
 * A fun��o deve retornar NULL
 */
DECLARE
BEGIN
    proc_testar_stock_artigo_armazem(1000, 'Artigo 4', NULL);
END;
/

/**
 * Teste que retorna a quantidade de stock de um artigo inextitente no armazem 5.
 * A fun��o deve retornar NULL
 */
DECLARE
BEGIN
    proc_testar_stock_artigo_armazem(5, 'Artigo 544', NULL);
END;
/
