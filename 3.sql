SET SERVEROUTPUT ON;

/**
 * 3. Implementar uma fun��o designada func_zona_maior_armazenamento, para obter um cursor com
 * os identificadores das zonas f�sicas de um dado armaz�m, que possuem a maior quantidade de
 * artigos em stock. A fun��o tem como par�metro o identificador do armaz�m. No caso de o
 * identificador do armaz�m n�o existir na BD ou a maior quantidade de artigos em stock for zero, a
 * fun��o deve retornar o valor NULL.
 */
CREATE OR REPLACE FUNCTION func_zona_maior_armazenamento(p_cod_armazem IN armazem.cod_armazem%TYPE)
RETURN SYS_REFCURSOR
IS
    l_cursor_id_zona_fisica_artigo  SYS_REFCURSOR;
    l_stock_max                     INTEGER :=0;
    l_cod_armazem                   INTEGER :=0;
    l_cod_armazem_inexistente       EXCEPTION;
    l_stock_max_nulo                EXCEPTION;
BEGIN

    -- Guarda na vari�vel l_cod_armazem, a quantidade de armaz�ns com o c�digo recebido por par�metro.
    SELECT COUNT(*) INTO l_cod_armazem
    FROM armazem A
    WHERE A.cod_armazem = p_cod_armazem;
    
    -- Se o c�digo do armaz�m n�o for encontrado na base de dados, � lan�ada a exce��o l_cod_armazem_inexistente.
    IF l_cod_armazem < 1 THEN
        RAISE l_cod_armazem_inexistente;
    END IF;
    
    -- Guarda na vari�vel l_stock_max, o valor m�ximo do stock de uma zona f�sica, que se encontra no armaz�m com o c�digo recebido por par�metro. 
    SELECT MAX(stock) INTO l_stock_max
    FROM zona_fisica_artigo ZFA
    WHERE ZFA.cod_armazem = p_cod_armazem;
    
    -- Se a maior quantidade de stock for 0, � lan�ada a exce��o l_stock_max_nulo.
    IF l_stock_max = 0 THEN
        RAISE l_stock_max_nulo;
    END IF;
    
    -- Guarda o id das zonas f�sicas, com o stock igual ao stock m�ximo guardado na vari�vel l_stock_max, e cujo c�digo 
    -- de armaz�m � igual ao recebido por par�metro, no cursor que vai ser retornado.
    OPEN l_cursor_id_zona_fisica_artigo FOR
        SELECT id_zona_fisica
        FROM zona_fisica_artigo ZFA
        WHERE ZFA.stock = l_stock_max
          AND ZFA.cod_armazem = p_cod_armazem;

    RETURN l_cursor_id_zona_fisica_artigo;
       
EXCEPTION   
    -- Esta exce��o � lan�ada quando o c�digo do armaz�m recebido por par�metro n�o � encontrado na base de dados, e retorna NULL.
    WHEN l_cod_armazem_inexistente THEN
        RETURN NULL;
        
    -- Esta exce��o � lan�ada quando o maior quantidade de stock � 0, e retorna NULL.
    WHEN l_stock_max_nulo THEN
        RETURN NULL;
END;
/

-- TESTES

/**
 * Procedimento utilizado para testar o cursor retornado na fun��o
 */
CREATE OR REPLACE PROCEDURE proc_id_zona_fisica(p_cursor_id_zona_fisica IN SYS_REFCURSOR)
IS
    l_id_zona_fisica    zona_fisica_artigo.id_zona_fisica%TYPE;
    l_cabecalho         VARCHAR(15);

    l_cursor_invalido   EXCEPTION;
BEGIN
    IF (p_cursor_id_zona_fisica IS NULL) THEN
        RAISE l_cursor_invalido;
    END IF;

    l_cabecalho := 'Id Zona F�sica';
    DBMS_OUTPUT.PUT_LINE(l_cabecalho);
    DBMS_OUTPUT.PUT_LINE(RPAD('-',LENGTH(l_cabecalho),'-'));
    
    LOOP
        FETCH p_cursor_id_zona_fisica INTO l_id_zona_fisica;
        EXIT WHEN p_cursor_id_zona_fisica%NOTFOUND;

        DBMS_OUTPUT.PUT_LINE(l_id_zona_fisica);
    END LOOP;

EXCEPTION
    WHEN l_cursor_invalido THEN
        DBMS_OUTPUT.PUT_LINE('NULL');
END;
/

/**
 * Teste que retorna um cursor com o id das zonas f�sicas com o maior stock de artigos, do armaz�m com o c�digo 1.
 * Deve retornar 'Zona A1'.
 */
DECLARE
BEGIN
    proc_id_zona_fisica(func_zona_maior_armazenamento(1));
END;
/

/**
 * Teste que retorna um cursor com o id das zonas f�sicas com o maior stock de artigos, do armaz�m com o c�digo 2.
 * Deve retornar 'Zona B2' e 'Zona C2'.
 */
DECLARE
BEGIN
    proc_id_zona_fisica(func_zona_maior_armazenamento(2));
END;
/

/**
 * Teste que retorna um cursor com o id das zonas f�sicas com o maior stock de artigos, do armaz�m com o c�digo 3.
 * Deve retornar 'NULL', pois a maior quantidade de stock do armazem � 0.
 */
DECLARE
BEGIN
    proc_id_zona_fisica(func_zona_maior_armazenamento(3));
END;
/

/**
 * Teste que retorna um cursor com o id das zonas f�sicas com o maior stock de artigos, do armaz�m com o c�digo 12.
 * Deve retornar 'NULL', pois o armaz�m n�o existe.
 */
DECLARE
BEGIN
    proc_id_zona_fisica(func_zona_maior_armazenamento(12));
END;
/   