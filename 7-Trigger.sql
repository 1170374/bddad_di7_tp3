SET SERVEROUTPUT ON;

/**
 * 7. Implementar um trigger designado trg_nota_encomenda_impedir_registo, que  impe�a  que  seja
 * registada uma  nota  de  encomenda de  um cliente  se  este  tiver  faturas  por  pagar
 * h�  mais  de tr�s meses.
 */
CREATE OR REPLACE TRIGGER trg_nota_encomenda_impedir_registo
BEFORE INSERT ON nota_encomenda 
FOR EACH ROW
BEGIN
    -- Se o retorno da fun��o for FALSE, o trigger lan�a um erro para que a nota de encomenda n�o seja inserida.
    IF (func_nota_encomenda_verificar_pagamentos(:NEW.cod_cliente)) = FALSE THEN
        RAISE_APPLICATION_ERROR(-20006, 'O cliente tem fatura por pagar h� mais de 3 meses.');
    END IF;
END;
/

-- TESTES

ALTER TRIGGER trg_nota_encomenda_impedir_registo ENABLE;

/**
 * Ativa o trigger quando � inserida uma nota_encomenda na base de dados.
 * Deve adicionar a nota_encomenda, pois o cliente n�o tem faturas por pagar h� mais de 3 meses.
 */
DECLARE
BEGIN
    INSERT INTO nota_encomenda VALUES('NE15', 1, 1, 'PR', DATE '2018-11-12');
    DELETE FROM nota_encomenda WHERE id_nota_encomenda='NE15';
END;
/

/**
 * Ativa o trigger quando � inserida uma nota_encomenda na base de dados.
 * Deve adicionar a nota_encomenda, pois o cliente n�o tem faturas por pagar h� mais de 3 meses.
 */
DECLARE
BEGIN
    INSERT INTO nota_encomenda VALUES('NE16', 1, 4, 'PR', DATE '2018-11-12');
    DBMS_OUTPUT.PUT_LINE('Nota encomenda registada com sucesso.');
    DELETE FROM nota_encomenda WHERE id_nota_encomenda='NE16';
END;
/

/**
 * Ativa o trigger quando � inserida uma nota_encomenda na base de dados.
 * N�o deve adicionar a nota_encomenda, pois o cliente n�o tem faturas.
 */
DECLARE
BEGIN
    INSERT INTO nota_encomenda VALUES ('NE17', 1, 5, 'PR', DATE '2018-12-01');
    DBMS_OUTPUT.PUT_LINE('Nota encomenda registada com sucesso.');
    DELETE FROM nota_encomenda WHERE id_nota_encomenda='NE17';
END;
/