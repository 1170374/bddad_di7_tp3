
/**
 * Conta as viagens feitas por um veiculo nos 7 dias anteriores � data enviada por parametro.
 */
CREATE OR REPLACE FUNCTION func_contar_viagens(p_matricula IN viagem.matricula%TYPE,
                                               p_data      IN viagem.data_partida%TYPE)
RETURN INTEGER
IS
    l_qtd_viagens        INTEGER;
    l_parametro_invalido EXCEPTION;
BEGIN
    IF p_matricula IS NULL OR p_data IS NULL THEN
        RAISE l_parametro_invalido;
    END IF;

    SELECT COUNT(*) INTO l_qtd_viagens
    FROM viagem V
    WHERE V.matricula = p_matricula
      AND V.data_partida BETWEEN p_data - 7 AND p_data;

    RETURN l_qtd_viagens;
EXCEPTION
    WHEN l_parametro_invalido THEN
        RETURN 0;
END;
/

-- Testes

SET SERVEROUTPUT ON;

/**
 * Procedimento para efetuar os varios testes.
 */
CREATE OR REPLACE PROCEDURE proc_testar_func_contar_viagens(p_matricula    IN viagem.matricula%TYPE,
                                                            p_data_partida IN viagem.data_partida%TYPE,
                                                            p_esperado     IN  INTEGER)
IS
    l_resultado       INTEGER;
    l_resultado_teste BOOLEAN;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Teste para a matricula "' || p_matricula || '" na data "' || p_data_partida
                         || '" onde o valor esperado � "' || p_esperado || '".');

    l_resultado := func_contar_viagens(p_matricula, p_data_partida);

    DBMS_OUTPUT.PUT_LINE('Resultado: ' || l_resultado);

    IF l_resultado IS NULL OR p_esperado IS NULL THEN
        l_resultado_teste := l_resultado IS NULL AND p_esperado IS NULL;
    ELSE
        l_resultado_teste := l_resultado = p_esperado;
    END IF;

    IF l_resultado_teste THEN
        DBMS_OUTPUT.PUT_LINE('Passou');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Falhou');
    END IF;
END;
/

/**
 * Teste para a matricula 14-GH-55 em que a data de partida � a data atual e o resultado esperado � 0.
 */
DECLARE
BEGIN
    proc_testar_func_contar_viagens('14-GH-55', SYSDATE, 0);
END;
/

/**
 * Teste para a matricula 14-GH-55 em que a data de partida � 2018-02-27 e o resultado esperado � 1.
 */
DECLARE
BEGIN
    proc_testar_func_contar_viagens('14-GH-55', DATE '2018-02-27', 1);
END;
/

/**
 * Teste para uma matricula invalida, em que o resultado esperado � 0.
 */
DECLARE
BEGIN
    proc_testar_func_contar_viagens('lalala', DATE '2018-02-27', 0);
END;
/

/**
 * Teste para uma matricula NULL, em que o resultado esperado � 0.
 */
DECLARE
BEGIN
    proc_testar_func_contar_viagens(NULL, DATE '2018-02-27', 0);
END;
/

/**
 * Teste para uma data NULL, em que o resultado esperado � 0.
 */
DECLARE
BEGIN
    proc_testar_func_contar_viagens('14-GH-55', NULL, 0);
END;
/

/**
 * Teste para uma matricula e data NULL, em que o resultado esperado � 0.
 */
DECLARE
BEGIN
    proc_testar_func_contar_viagens(NULL, NULL, 0);
END;
/
