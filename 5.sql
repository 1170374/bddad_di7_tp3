
SET SERVEROUTPUT ON;

/**
 * 5. Implementar um procedimento designado proc_tratar_nota_encomenda, para processar uma dada
 * nota de encomenda. O procedimento tem como parâmetro o identificador da nota de encomenda.
 * Caso não seja possível processar a nota de encomenda, deve ser armazenada toda a informação de
 * todos os produtos em falta. No entanto, caso seja possível processar a nota de encomenda, deve ser
 * atualizada a informação relativa ao armazém, e zona física, de onde o artigo vai sair retirado. Para
 * além disso deve ser gerada a respetiva guia de saída. No caso de o identificador da nota de
 * encomenda não existir na BD o procedimento deve lançar uma exceção adequada. Se se entender
 * necessário podem ser realizadas alterações ao modelo relacional.
 */
CREATE OR REPLACE PROCEDURE proc_tratar_nota_encomenda(p_id_nota_encomenda nota_encomenda.id_nota_encomenda%TYPE)
IS

    -- Contador de notas de encomenda para efeito de validacao.
    l_cont_notas            INTEGER;
    l_cont_guias            INTEGER;

    -- Contador de artigos em falta, que serve como flag para verificar se e preciso criar a guia de saida.
    l_cont_artigos_em_falta INTEGER;

    -- Serve para somar/subtrair stock.
    l_stock                 zona_fisica_artigo.stock%TYPE;

    -- Indica o stock a retirar de uma zona_fisica_artigo.
    l_stock_a_retirar       zona_fisica_artigo.stock%TYPE;

    -- O id da guia de saida gerada.
    l_id_guia_saida         guia_saida.id_guia_saida%TYPE;

    -- Todos os artigos de uma encomenda.
    CURSOR l_cur_artigos_encomenda IS
        SELECT *
        FROM artigo_encomenda
        WHERE id_nota_encomenda LIKE p_id_nota_encomenda;
    l_artigo_encomenda      l_cur_artigos_encomenda%ROWTYPE;

    -- Todas as zonas fisicas de um artigo.
    CURSOR l_cur_zonas_artigo(l_p_referencia_artigo zona_fisica_artigo.referencia_artigo%TYPE) IS
        SELECT *
        FROM zona_fisica_artigo
        WHERE referencia_artigo = l_p_referencia_artigo;
    l_zona_artigo           l_cur_zonas_artigo%ROWTYPE;

    -- Exceções.
    l_id_invalido           EXCEPTION;
    l_guia_ja_existe        EXCEPTION;

BEGIN

    -- Validar o id da nota de encomenda.
    SELECT COUNT(*) INTO l_cont_notas
    FROM nota_encomenda
    WHERE id_nota_encomenda = p_id_nota_encomenda;

    IF l_cont_notas = 0 THEN
        RAISE l_id_invalido;
    END IF;

    SELECT COUNT(*) INTO l_cont_guias
    FROM guia_saida
    WHERE id_nota_encomenda = p_id_nota_encomenda;

    IF l_cont_guias != 0 THEN
        RAISE l_guia_ja_existe;
    END IF;

    -- Percorrer todos os artigos da nota de encomenda e verificar se ha stock suficiente nas zonas fisicas.
    l_cont_artigos_em_falta := 0; -- Flag para posteriormente verificar se deve ser gerada a guia de saida.
    FOR l_artigo_encomenda IN l_cur_artigos_encomenda LOOP

        -- Averiguar o stock do artigo possuido em todas as zonas fisicas.
        l_stock := 0;
        FOR l_zona_artigo IN l_cur_zonas_artigo(l_artigo_encomenda.referencia_artigo) LOOP
            l_stock := l_stock + l_zona_artigo.stock;
        END LOOP;

        -- Se o stock de todas as zonas for inferior à quantidade pedida, a guia de saida nao pode ser criada.
        IF l_stock < l_artigo_encomenda.quantidade THEN
            INSERT INTO produtos_falta VALUES (l_artigo_encomenda.referencia_artigo,
                                               p_id_nota_encomenda,
                                               l_artigo_encomenda.quantidade - l_stock);
            l_cont_artigos_em_falta := l_cont_artigos_em_falta + 1;
        END IF;
    END LOOP;

    -- Se houver artigos em falta, sair.
    IF l_cont_artigos_em_falta > 0 THEN
        RETURN;
    END IF;

    -- Criar a guia de saida e o trigger do exercicio 9 encarrega-se de tratar dos artigos da guia.
    INSERT INTO guia_saida VALUES (DEFAULT, p_id_nota_encomenda);

EXCEPTION
    WHEN l_id_invalido THEN
        DBMS_OUTPUT.PUT_LINE('O id da nota de encomenda nao foi encontrado');
    WHEN l_guia_ja_existe THEN
        DBMS_OUTPUT.PUT_LINE('Já existe uma guia de saida para esta nota de encomenda');
END;
/

/**
 * É esperado que apareça uma mensagem a informar que já existe uma guia de saida para esta nota de encomenda.
 */
DECLARE
BEGIN
    proc_tratar_nota_encomenda('NE1');
END;
/

/**
 * Neste teste, é esperado que sejam geradas varios registos em guia_artigo_zona com as quantidades da nota de encomenda
 * NE2.
 * É esperado que sejam criados os seguintes elementos de guia_artigo_zona:
 * Na Zona B2 do armazem 2, 10 do artigo 2.
 * Na Zona B2 do armazem 2, 19 do artigo 4.
 * Na Zona B2 do armazem 4, 45 do artigo 3.
 * Na Zona B2 do armazem 5, 155 do artigo 3.
 */
DECLARE
    l_guia_artigo_zona guia_artigo_zona%ROWTYPE;
BEGIN
    proc_tratar_nota_encomenda('NE2');

    FOR l_guia_artigo_zona IN (SELECT GAZ.id_guia_saida, GAZ.id_zona_fisica, GAZ.referencia_artigo, GAZ.cod_armazem, GAZ.quantidade
                               FROM guia_artigo_zona GAZ INNER JOIN guia_saida S ON GAZ.id_guia_saida = S.id_guia_saida
                               WHERE S.id_nota_encomenda LIKE 'NE2') LOOP
        DBMS_OUTPUT.PUT_LINE('id_guia_saida: '           || l_guia_artigo_zona.id_guia_saida
                             || ' | id_zona_fisica: '    || l_guia_artigo_zona.id_zona_fisica
                             || ' | referencia_artigo: ' || l_guia_artigo_zona.referencia_artigo
                             || ' | cod_armazem: '       || l_guia_artigo_zona.cod_armazem
                             || ' | quantidade: '        || l_guia_artigo_zona.quantidade);
    END LOOP;

    DELETE FROM guia_saida
    WHERE id_nota_encomenda LIKE 'NE2';
END;
/

/**
 * Neste teste, é esperado que não seja gerada a guia de saida e que sejam criados artigos em falta.
 * Devem ser criados os seguintes elementos de produtos em falta:
 * Artigo 1: 200 em falta
 * Artigo 2: 321 em falta
 * Artigo 3: 102 em falta
 */
DECLARE
    l_cont_guias_saida INTEGER;
    l_guia_artigo_zona guia_artigo_zona%ROWTYPE;
    l_produto_falta    produtos_falta%ROWTYPE;
BEGIN
    proc_tratar_nota_encomenda('NE10');

    SELECT COUNT(*) INTO l_cont_guias_saida
    FROM guia_saida
    WHERE id_nota_encomenda LIKE 'NE10';

    IF l_cont_guias_saida > 0 THEN
        DBMS_OUTPUT.PUT_LINE('O teste falhou porque foi gerada uma guia de saida.');
        DBMS_OUTPUT.PUT_LINE('Foram (incorretamente) gerados os seguintes elementos de guia_artigo_zona:');

        FOR l_guia_artigo_zona IN (SELECT GAZ.id_guia_saida, GAZ.id_zona_fisica, GAZ.referencia_artigo, GAZ.cod_armazem, GAZ.quantidade
                                   FROM guia_artigo_zona GAZ INNER JOIN guia_saida S ON GAZ.id_guia_saida = S.id_guia_saida
                                   WHERE S.id_nota_encomenda LIKE 'NE10') LOOP
            DBMS_OUTPUT.PUT_LINE('id_guia_saida: '           || l_guia_artigo_zona.id_guia_saida
                                 || ' | id_zona_fisica: '    || l_guia_artigo_zona.id_zona_fisica
                                 || ' | referencia_artigo: ' || l_guia_artigo_zona.referencia_artigo
                                 || ' | cod_armazem: '       || l_guia_artigo_zona.cod_armazem
                                 || ' | quantidade: '        || l_guia_artigo_zona.quantidade);
        END LOOP;
    END IF;


    DBMS_OUTPUT.PUT_LINE('Foram criados os seguintes elementos de produtos_falta:');
    FOR l_produto_falta IN (SELECT *
                            FROM produtos_falta
                            WHERE id_nota_encomenda LIKE 'NE10') LOOP
        DBMS_OUTPUT.PUT_LINE('referencia_artigo: '       || l_produto_falta.referencia_artigo
                             || ' | id_nota_encomenda: ' || l_produto_falta.id_nota_encomenda
                             || ' | quantidade_falta: '  || l_produto_falta.quantidade_falta);
    END LOOP;

    DELETE FROM guia_saida
    WHERE id_nota_encomenda LIKE 'NE2';
END;
/

/**
 * Neste teste é suposto que apareça uma mensagem a informar que o id da nota de encomenda não foi encontrado.
 */
DECLARE
BEGIN
    proc_tratar_nota_encomenda('qwertyuiopasdfghjklzxcvbnm');
END;;
/

/**
 * Neste teste é suposto que apareça uma mensagem a informar que o id da nota de encomenda não foi encontrado.
 */
DECLARE
BEGIN
    proc_tratar_nota_encomenda(NULL);
END;
/
